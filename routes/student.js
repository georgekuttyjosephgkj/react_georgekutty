/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/

var express = require('express');
var router = express.Router();
var httpOperations = require('../modules/httpOperations.js');

var academicSessionReport=require('./hardCodedResponses/academicSessionReport.json');
var admissionRegReportResponse=require('./hardCodedResponses/admissionRegReportResponse.json');

router.get('/academicSessionReport', function(req, res, next) {
  // httpOperations.executeAxiosGet("http://app.samvardhanngo.org:5000/api/academic-session/getreport",function(response){
  //   console.log("academicSessionReport res >>",response.data);
  //   res.send(response.data);
  // })
  setTimeout(function(){
    res.send(academicSessionReport);
  },2000);
  
});

router.post('/admissionRegReport', function(req, res, next) {
  console.log("academicYear body>>",req.body);
  // httpOperations.executeAxiosGet("http://app.samvardhanngo.org:5000/api/admission-registration/getreport",function(response){
  //   console.log("admissionRegReport res >>",response.data);
  //   res.send(response.data);
  // })
  setTimeout(function(){
    res.send(admissionRegReportResponse);
  },2000);
});

router.post('/promote', function(req, res, next) {
  console.log("promote body>>",req.body);
  setTimeout(function(){
    res.send({status:"success"});
  },2000);
});

module.exports = router;
