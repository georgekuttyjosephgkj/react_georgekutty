/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/

var express = require('express');
var router = express.Router();
var httpOperations = require('../modules/httpOperations.js');


router.post('/add', function (req, res, next) {
    console.log("add body>>", req.body);
    setTimeout(function () {
        res.send({ status: "success" });
    }, 2000);
});

router.post('/update', function (req, res, next) {
    console.log("update body>>", req.body);
    setTimeout(function () {
        res.send({ status: "success" });
    }, 2000);
});

router.get('/getData', function (req, res, next) {
    console.log("getData body>>", req.body);
    setTimeout(function () {
        res.send({ status: "success" });
    }, 2000);
});

module.exports = router;
