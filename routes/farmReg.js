/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/

var express = require('express');
var router = express.Router();
var httpOperations = require('../modules/httpOperations.js');


router.post('/add', function (req, res, next) {
    console.log("add body>>", req.body);
    setTimeout(function () {
        res.send({ status: "success" });
    }, 2000);
});

router.post('/update/:id', function (req, res, next) {
    console.log("update body>>", req.body);
    setTimeout(function () {
        res.send({ status: "success" });
    }, 2000);
});

router.get('/getData/:id', function (req, res, next) {
    console.log("getData body>>", req.body);
    setTimeout(function () {
        res.send({
            "data": {
              "count": 1,
              "rows": [
                {
                  "id": 1,
                  "product_id": 12345,
                  "sub_product_id": 1,
                  "place_name": "pune",
                  "gat_no": "220",
                  "date": "2018-07-13T11:03:42.000Z",
                  "owner_name": "amole",
                  "contact_person": "amole",
                  "contact_no": "1212121212",
                  "area": "banglore",
                  "land_type": "type1",
                  "ownership_type": "type2",
                  "address": "abcd",
                  "legal_text": "",
                  "status": "",
                  "created_by": 0,
                  "createdAt": "2018-07-13T11:03:42.000Z",
                  "updatedAt": null,
                  "deletedAt": null
                }
              ]
            },
            "count": 1,
            "pages": 1
          });
    }, 2000);
});

module.exports = router;
