/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/

var express = require('express');
var router = express.Router();
var httpOperations = require('../modules/httpOperations.js');


router.post('/submitDischarge', function (req, res, next) {
    console.log("submitDischarge body>>", req.body);
    setTimeout(function () {
        res.send({ status: "success" });
    }, 2000);
});

module.exports = router;
