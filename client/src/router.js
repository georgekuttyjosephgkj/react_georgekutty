import React from 'react';
import { Route, HashRouter, } from 'react-router-dom';
import './res/scss/main.scss';

import MasterFee from './scenes/feeSection/views/master-fee';

export default (
  <HashRouter>
      <Route path="/master-fee"  component={MasterFee} />
  </HashRouter>
);
