import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter } from 'react-router-dom';
import { AppContainer } from 'react-hot-loader';
import reducers from './reducers/index.js';
import App from './app';
import './index.css';
import Router from './router';
import registerServiceWorker from './registerServiceWorker';
import thunk from 'redux-thunk';


const initialState = window.__INITIAL_STATE__; // eslint-disable-line
const store = createStore(reducers, initialState, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('reactbody'));
registerServiceWorker();

  // ReactDOM.render(
  //   <AppContainer>
  //     <Provider store={store}>
  //       <BrowserRouter>
  //         {Router}
  //       </BrowserRouter>
  //     </Provider>
  //   </AppContainer>,
  //   document.getElementById('reactbody'),
  // );


// if (module.hot) {
//   module.hot.accept('./app', () => {
//     // eslint-disable-next-line
//     const nextApp = require('./app').default;
//     render(nextApp);
//   });
// }

// module.hot.accept('./reducers', () => {
//   // eslint-disable-next-line
//   const nextRootReducer = require('./reducers/index');
//   store.replaceReducer(nextRootReducer);
// });
