import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import { Provider } from 'react-redux';
import Student_fee_payment from './scenes/feeSection/views/master-fee';
import HallInfoComponent from './scenes/HallInfo';
import DischargForm from './scenes/DischargForm'
import ApmntForm from './scenes/ApmntForm'
import PromotionForm from './scenes/Promotion'
import QualificationForm from './scenes/Qualification'
import RoleForm from './scenes/Role.js'

import FarmReg from './scenes/FarmReg'
import FarmPlotReg from './scenes/FarmPlotReg'

import Temp from './scenes/Temp'



import './res/scss/sweetalert.css';
import 'react-virtualized/styles.css'

const App = () => (
    <Router>
      <Switch>
        <Route exact path="/" component={Student_fee_payment} />
        <Route exact path="/library/:id" component={HallInfoComponent} />
        <Route exact path="/library" component={HallInfoComponent} />
        <Route exact path="/dischargForm" component={DischargForm} />
        <Route exact path="/appoinmentForm" component={ApmntForm} />
        <Route exact path="/appoinmentForm/:id" component={ApmntForm} />
        <Route exact path="/farmReg" component={FarmReg} />
        <Route exact path="/farmReg/:id" component={FarmReg} />
        <Route exact path="/farmPlotReg" component={FarmPlotReg} />
        <Route exact path="/farmPlotReg/:id" component={FarmPlotReg} />
        <Route exact path="/temp" component={Temp} />
        <Route exact path="/promotion" component={PromotionForm} />
        <Route exact path="/qualification" component={QualificationForm} />
        <Route exact path="/role" component={RoleForm} />
      </Switch>
    </Router>
);

export default App;
