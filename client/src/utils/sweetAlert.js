/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/

import React from 'react';
import ReactSwal from 'react-swal';

export function SweetAlert(props) {
    return (
        <ReactSwal         
            isOpen={ props.isVisible}
            title={ props.title}
            type={ props.type}
            text={ props.text}
            timer={2000}
            showConfirmButton={ props.showConfirmButton}
            confirmButtonText="OK"
            showCancelButton={ props.showCancelButton}
            cancelButtonText="CANCEL"
            callback={ props.callback}/>
    );
}


