/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/

import React from 'react';
import ReactLoading from 'react-loading';

//type : blank,balls,bars,bubbles,cubes,cylon,spin,spinningBubbles,spokes
var styles={
    solidLoaderContainer:{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'fixed',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
    },
    transparentLoaderContainer:{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'fixed',
        top: 0,
        right: 0,
        left: 0,
        bottom:0,
        backgroundColor: 'rgba(0, 0, 0, 0.078)'
    }
}

export function SolidLoadingAnimation(props) {
    return (
        <div style={styles.solidLoaderContainer}>
            <ReactLoading type="bars" color="#000" delay={50} height='50px' width='50px' />
        </div>
        );
}

export function TransparentLoadingAnimation(props) {
    if(props.isVisible){
        return (
            <div style={styles.transparentLoaderContainer}>
                <ReactLoading type="bars" color="#265a88" delay={50} height='50px' width='50px'/>
            </div>
        );
    }else{
        return null
    }
}


