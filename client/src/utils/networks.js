/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/
import axios from 'axios';

export function executeAxios(url, methodType, data, callBack) {
  if (methodType == "GET") {
    axios.get(url)
      .then(res => {
        callBack(res.data)
      }).catch((ex) => {
        console.log('parsing failed', ex)
      })
  } else if (methodType == "POST") {
    axios.post(url,data)
      .then(res => {
        callBack(res.data)
      }).catch((ex) => {
        console.log('parsing failed', ex)
      })
  }
}


