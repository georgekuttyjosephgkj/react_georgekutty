import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';
//import {getDataList} from '../organization/action/organization-campus-action';
import 'react-datepicker/dist/react-datepicker.css';
import '../res/scss/form.css';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import {
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock,
    Button
} from 'react-bootstrap';

const checkboxList = [{ id: '1', role: 'Organization' }, { id: '2', role: 'Hr' }, { id: '3', role: 'ab' }];
const Checkbox = ({ id = id, type = 'checkbox', name, checked = false, onChange }) => (
    <input id={id} type={type} name={name} checked={checked} onChange={onChange} />
);

Checkbox.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
}

class OrganizationRole extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isSubmitted: false,
            employeeId: '4000',//''
            roles: "1,2"
        }
    }

    componentWillReceiveProps = (nextProps) => {
        console.log('Called', nextProps)
        if (nextProps.isUpdateData && nextProps.isUpdateData.id && !this.state.isUpdateSet) {
            const isUpdateData = nextProps.isUpdateData;
            this.setState({
                employeeId: isUpdateData.employeeId,
                roles: isUpdateData.roles,
                isUpdateSet: true,
            });
        }
    };

    componentDidMount() {
        //this.props.getDataList('Hr_Employee_Registration',this.props.controlObject)
    }

    handleStateChange = (key, event) => {
        var receivedKey=key;
        var receivedName=event.target.name;
        var receivedValue=event.target.value;
        var receivedId=event.target.id;
        var receivedChecked=event.target.checked;
        var finalValue="";
        console.log(">>>"+receivedKey+" : "+receivedName+" : "+receivedValue+" : "+receivedId+" : "+receivedChecked);
        if(receivedKey=="employeeId"){
            finalValue=receivedValue;
        }else if(receivedKey=="roles"){
            var prevRolesArray=[];
            if((this.state.roles).split(",") != "" ){
                prevRolesArray=(this.state.roles).split(",");
            }
            if(receivedChecked){//true
                prevRolesArray.push(receivedId);
            }else{
                var currentIndex=prevRolesArray.indexOf(receivedId);
                prevRolesArray.splice(currentIndex,1);
            }
            finalValue=prevRolesArray.toString();
        }
        this.setState({[receivedKey]: finalValue})
        setTimeout(()=>{
            console.log("this.state >>",this.state);
            console.log("receivedKey:finalValue >> "+receivedKey+":"+finalValue);
            //this.props.handleChildValue({ [receivedKey]: finalValue });
        },500);
    };


    render() {
        return (
            <div className="formWrapper">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="col-sm-12">
                            <div className="col-sm-12">
                                <div className="form-header">
                                    <div className="col-sm-6 no-padding">
                                        <h3 style={{color: "#000"}}>Organization Roles</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12">
                            <form className="info-form">
                                <div className="col-sm-4">
                                    <label>Employee Id</label>
                                    <select className="form-control" name='employeeId' value={this.state.employeeId} onChange={(e) => this.handleStateChange('employeeId', e)}  >
                                        <option key={'-1'} value={'-1'}>Select</option>
                                        <option key={'1000'} value={'1000'}>1000</option>
                                        <option key={'2000'} value={'2000'}>2000</option>
                                        <option key={'3000'} value={'3000'}>3000</option>
                                        <option key={'4000'} value={'4000'}>4000</option>
                                        {/* {this.props.organizationCampusResponse.success ?
                                            this.props.organizationCampusResponse.DataList.map(datalist => datalist === null ? '' : <option key={datalist.id} value={datalist.id}>{datalist.id} - {datalist.name}</option>)
                                            : ''
                                        } */}
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div className="col-sm-12">
                            <form className="info-form">
                                {
                                    checkboxList.map(item => (
                                        <div className="col-sm-4">
                                            <Checkbox id={item.id} name={item.role} checked={(this.state.roles).indexOf(item.id) == -1?false:true  } onChange={(e)=>this.handleStateChange("roles",e)} />
                                            <label htmlFor={item.role} key={item.id}>
                                                {item.role}
                                            </label>
                                        </div>
                                    ))
                                }
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    organizationCampusResponse: state.organizationCampusResponse,
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        // getDataList: (api,currentuser) => {
        //   dispatch(getDataList(api,currentuser));
        // }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(OrganizationRole);

























// import React, { Component } from 'react'
// import DatePicker from 'react-datepicker';
// import moment from 'moment';
// //import {getDataList} from '../organization/action/organization-campus-action';
// import 'react-datepicker/dist/react-datepicker.css';
// import '../res/scss/form.css';
// import { connect } from "react-redux";
// import PropTypes from 'prop-types';
// import {
//     FormGroup,
//     FormControl,
//     ControlLabel,
//     HelpBlock,
//     Button
// } from 'react-bootstrap';

// const checkboxList = [{ id: 1, role: 'Organization' }, { id: 2, role: 'Hr' }, { id: 3, role: 'ab' }];
// const Checkbox = ({ id = id, type = 'checkbox', name, checked = false, onChange }) => (
//     <input id={id} type={type} name={name} checked={checked} onChange={onChange} />
// );

// Checkbox.propTypes = {
//     type: PropTypes.string,
//     name: PropTypes.string.isRequired,
//     checked: PropTypes.bool,
//     onChange: PropTypes.func.isRequired,
// }

// class OrganizationRole extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             isSubmitted: false,
//             employeeId: '',
//             roles: [],
//             //roles: "",
//             //checkedItems: new Map(),
//         }
//         //this.handleChange = this.handleChange.bind(this);
//     }

//     componentWillReceiveProps = (nextProps) => {
//         //This will call every time props update
//         // when passing state from parent it will become props to child
//         //tht means it will update this part
//         //but this one is not called don't know why 
//         console.log('Called', nextProps)
//         if (nextProps.isUpdateData && nextProps.isUpdateData.id && !this.state.isUpdateSet) {
//             const isUpdateData = nextProps.isUpdateData;
//             this.setState({
//                 employeeId: isUpdateData.employeeId,
//                 roles: isUpdateData.roles,
//                 isUpdateSet: true,
//             });
//         }
//     };

//     componentDidMount() {
//         //this.props.getDataList('Hr_Employee_Registration',this.props.controlObject)
//     }

//     // handleChange(e) {
//     //     //debugger
//     //     const item = e.target.name;
//     //     const itemId = e.target.id;
//     //     const isChecked = e.target.checked;
//     //     // this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item, isChecked) }));
//     //     this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item, isChecked) }));
//     //     if (isChecked) {
//     //         this.state.roles=  this.state.roles+","+itemId;
//     //     }
//     //     // var keys = Object.keys(checkedItems);
//     //     // var filtered = keys.filter(function(key) {
//     //     //  return Array.from(checkedItems.keys())[key]
//     //     // });
//     //     console.log("Get => ", item, isChecked, this.state, Array.from(this.state.checkedItems.keys()), this.state.roles)
//     // }

//     handleStateChange = (key, event) => {
//         var receivedKey=key;
//         var receivedValue=event.target.value;
//         var receivedId=event.target.id;
//         console.log(">>>"+receivedKey+" : "+receivedValue);

//         if(key=="employeeId"){
//             this.setState({[receivedKey]: receivedValue})
//         }else if(key=="role"){
//             this.state.roles.push(receivedId);
//             this.setState({"":""});
//             //this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(event.target.id, event.target.checked) }));
//         }
//         setTimeout(()=>{
//             console.log("state >>",this.state);
//         },500);
//         //this.props.handleChildValue({ [key]: value });
//     };

//     // handleCheck = (e) => {
//     //     console.log(e.target);
//     //     this.setState({
//     //         checked: e.target.checked
//     //     });
//     // }

//     render() {
//         return (
//             <div className="formWrapper">
//                 <div className="row">
//                     <div className="col-sm-12">
//                         <div className="col-sm-12">
//                             <div className="col-sm-12">
//                                 <div className="form-header">
//                                     <div className="col-sm-6 no-padding">
//                                         <h3 style={{color: "#000"}}>Organization Roles</h3>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>
//                         <div className="col-sm-12">
//                             <form className="info-form">
//                                 <div className="col-sm-4">
//                                     <label>Employee Id</label>
//                                     <select className="form-control" name='employeeId' onChange={(e) => this.handleStateChange('employeeId', e)}  >
//                                         <option key={'-1'} value={'-1'}>Select</option>
//                                         <option key={'1000'} value={'1000'}>1000</option>
//                                         <option key={'2000'} value={'2000'}>2000</option>
//                                         <option key={'3000'} value={'3000'}>3000</option>
//                                         <option key={'4000'} value={'4000'}>4000</option>
//                                         {/* {this.props.organizationCampusResponse.success ?
//                                             this.props.organizationCampusResponse.DataList.map(datalist => datalist === null ? '' : <option key={datalist.id} value={datalist.id}>{datalist.id} - {datalist.name}</option>)
//                                             : ''
//                                         } */}
//                                     </select>
//                                 </div>

//                             </form>
//                         </div>
//                         <div className="col-sm-12">
//                             <form className="info-form">
//                                 {
//                                     checkboxList.map(item => (
//                                         <div className="col-sm-4">
//                                             {/* <Checkbox id={item.id} name={item.role} checked={this.state.checkedItems.get(item.role)} onChange={this.handleChange} /> */}
//                                             <Checkbox id={item.id} name={item.role} checked={(this.state.roles).indexOf(item.id) ==-1?false:true} onChange={(e)=>this.handleStateChange("role",e)} />
//                                             <label htmlFor={item.role} key={item.id}>
//                                                 {item.role}
//                                             </label>
//                                         </div>
//                                     ))
//                                 }
//                             </form>
//                         </div>
//                     </div>
//                 </div>
//             </div>


//         )
//     }
// }

// const mapStateToProps = state => ({
//     organizationCampusResponse: state.organizationCampusResponse,
// })

// const mapDispatchToProps = (dispatch, props) => {
//     return {
//         // getDataList: (api,currentuser) => {
//         //   dispatch(getDataList(api,currentuser));
//         // }
//     }
// }


// export default connect(mapStateToProps, mapDispatchToProps)(OrganizationRole);

