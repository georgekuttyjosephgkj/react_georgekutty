import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import '../res/scss/form.css';

import {
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock,
    Button,

} from 'react-bootstrap';

import { connect } from 'react-redux';
import { getFarmReg, updateFarmReg, addFarmReg } from '../actions/farmReg_actions';
import { SolidLoadingAnimation, TransparentLoadingAnimation } from '../utils/loadingAnimation.js'

class FarmReg extends Component {
    constructor(props) {
        super(props)

        var isThisEditVisit = false;
        if (this.props.match.params.id) {
            isThisEditVisit = true;
        }

        this.state = {
            date: moment(),
            placeName: "",
            gatNumber: "",
            owner: "",
            contactPerson: "",
            contactNo: "",
            area: "",
            typeOfLand: "",
            typeOfOwnership: "",
            address: "",

            isThisEdit: isThisEditVisit,
            isThisFirstTime: true,
            isSubmitted: false
        };
    }

    componentWillMount() {
        if (this.state.isThisEdit) {
            console.log("id is there>>" + this.state.isThisEdit);
            this.props.getFarmReg({ id: this.props.match.params.id });
        } else {
            console.log("id is not there>>" + this.state.isThisEdit);
        }
    }

    handleChange(from, date) {
        console.log("from date", from, " ", date);
        this.setState({
            [from]: date
        });
    }

    validateAllFields() {
        var statusToReturn = true;
        var idsOfFieldsToValidate=["placeName","gatNumber"]
        for(var i=0;i<idsOfFieldsToValidate.length;i++){
            var currentElement=document.getElementById(idsOfFieldsToValidate[i]);
            if(currentElement.value != "" && currentElement.value != "select"){//|| currentElement.value != "select"
                console.log("ok >> "+idsOfFieldsToValidate[i]+" >>"+currentElement.value);
                currentElement.setAttribute("style","border:dummy")
            }else{
                console.log("not ok >> "+idsOfFieldsToValidate[i]+" >>"+currentElement.value);
                currentElement.setAttribute("style","border: 1px solid #d44545")
                statusToReturn = false;
            }
        }
        return statusToReturn;
    }

    handleUserInputs(type, newValue) {
        console.log(type+" "+newValue)
        this.state[type] = newValue;
        this.setState({ dummy: "" });
    }

    submitButtonClicked() {
        if (this.validateAllFields()) {
            this.setState({ isSubmitted: true });
            var dataToSend = {
                date: this.state.date.format("DD-MM-YYYY HH:mm:ss"),
                place_name: this.state.placeName,
                gat_no: this.state.gatNumber,
                owner_name: this.state.owner,
                contact_person: this.state.contactPerson,
                contact_no: this.state.contactNo,
                area: this.state.area,
                land_type: this.state.typeOfLand,
                ownership_type: this.state.typeOfOwnership,
                address: this.state.address,
                legal_text:this.state.legalText,
            }
            console.log("dataToSend is >>", dataToSend);
            if (this.state.isThisEdit) {
                console.log("edit submitted dataToSend >>", dataToSend);
                this.props.updateFarmReg(this.props.match.params.id, dataToSend);
            } else {
                console.log("add submitted dataToSend >>", dataToSend);
                this.props.addFarmReg(dataToSend);
            }
        } else {
            alert("please fill forms");
        }
    }




    render() {

        if (this.props.getFarmRegResponse.success == true && this.state.isThisFirstTime) {
            this.state.isThisFirstTime = false
            var responseObject=this.props.getFarmRegResponse.responseData.data.rows[0];
            console.log(" getFarmRegResponse >>" ,responseObject );
            this.state.date = moment(responseObject.date);
            this.state.placeName = responseObject.place_name;
            this.state.gatNumber = responseObject.gat_no;
            this.state.owner = responseObject.owner_name;
            this.state.contactPerson = responseObject.contact_person;
            this.state.contactNo = responseObject.contact_no;
            this.state.area = responseObject.area;
            this.state.typeOfLand = responseObject.land_type;
            this.state.typeOfOwnership = responseObject.ownership_type;
            this.state.address = responseObject.address;
            this.state.legalText=responseObject.legal_text;
        }


        if (this.props.addFarmRegResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("addFarmRegResponse success");
        }

        if (this.props.updateFarmRegResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("updateFarmRegResponse success");
        }



        return (
            <div className="formWrapper">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="col-sm-12">
                            <div className="form-header">
                                <h3>FARM REGISTRATION</h3>
                                <span className="subhead">Farm Information</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <form className="info-form">
                            <div className="col-sm-4">
                                <label>Place Name</label>
                                <select id="placeName" className="form-control" value={this.state.placeName} onChange={(event) => this.handleUserInputs("placeName", event.target.value)} >
                                    <option value="select">Select</option>
                                    <option value="place1">Place1</option>
                                    <option value="pune">pune</option>
                                </select>
                            </div>
                            <div className="col-sm-4">
                                <label>GAT Number</label>
                                <input id="gatNumber" value={this.state.gatNumber} onChange={(event) => this.handleUserInputs("gatNumber", event.target.value)} type="text" className="form-control" placeholder="GAT Number"/>
                            </div>
                            <div className="col-sm-4">
                                <label>Date</label>
                                <DatePicker className="form-control"
                                    selected={this.state.date}
                                    onChange={this.handleChange.bind(this, 'date')}
                                />
                            </div>
                            <div className="col-sm-4">
                                <label>Owner</label>
                                <input  value={this.state.owner} onChange={(event) => this.handleUserInputs("owner", event.target.value)} type="text" className="form-control" placeholder="Owner"/>
                            </div>
                            <div className="col-sm-4">
                                <label>Contact Person</label>
                                <input  value={this.state.contactPerson} onChange={(event) => this.handleUserInputs("contactPerson", event.target.value)} type="text" className="form-control" placeholder="Contact Person"/>
                            </div>
                            <div className="col-sm-4">
                                <label>Contact No.</label>
                                <input  value={this.state.contactNo} onChange={(event) => this.handleUserInputs("contactNo", event.target.value)} type="text" className="form-control" placeholder="Contact No."/>
                            </div>
                            <div className="col-sm-4">
                                <label>Area (Hectare)</label>
                                <input  value={this.state.area} onChange={(event) => this.handleUserInputs("area", event.target.value)} type="text" className="form-control" placeholder="Area (Hectare)"/>
                            </div>
                             <div className="col-sm-4">
                                <label>Type Of Land</label>
                                <select className="form-control"  value={this.state.typeOfLand} onChange={(event) => this.handleUserInputs("typeOfLand", event.target.value)}>
                                    <option value="select">Select</option>
                                    <option value="type1">Type1</option>
                                    <option value="type2">Type2</option>
                                </select>
                            </div>
                            <div className="col-sm-4">
                                <label>Type Of Ownership</label>
                                <select className="form-control" value={this.state.typeOfOwnership} onChange={(event) => this.handleUserInputs("typeOfOwnership", event.target.value)}>
                                    <option value="select">Select</option>
                                    <option value="type1">Type1</option>
                                    <option value="type2">Type2</option>
                                </select>
                            </div>
                            <div className="col-sm-4">
                                <label>Address</label>
                                <textarea value={this.state.address} onChange={(event) => this.handleUserInputs("address", event.target.value)} className="form-control" placeholder="Address"></textarea>
                            </div>
                            <div className="col-sm-4">
                                <label>Legal Text</label>
                                <input  value={this.state.legalText} onChange={(event) => this.handleUserInputs("legalText", event.target.value)} type="text" className="form-control" placeholder="Legal Text"/>
                            </div>
                            <div className="col-sm-12" style={{ justifyContent: "center", flex: 1, display: "flex", flexDirection: "column", alignItems: "center", padding: 40 }}>
                                <Button type="submit" bsStyle="primary" form="form" onClick={() => this.submitButtonClicked()} style={{ width: 100, color: "#fff" }}>Submit</Button>
                            </div>
                        </form>
                    </div>
                </div>    
                <TransparentLoadingAnimation isVisible={this.props.addFarmRegResponse.loading || this.props.getFarmRegResponse.loading || this.props.updateFarmRegResponse.loading} />            
            </div>
        )
    }
}

const mapStateToProps = state => ({
    getFarmRegResponse: state.getFarmRegResponse,
    addFarmRegResponse: state.addFarmRegResponse,
    updateFarmRegResponse: state.updateFarmRegResponse
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        getFarmReg: (dataToSend) => {
            dispatch(getFarmReg(dataToSend))
        },
        addFarmReg: (dataToSend) => {
            dispatch(addFarmReg(dataToSend))
        },
        updateFarmReg: (id, dataToSend) => {
            dispatch(updateFarmReg(id, dataToSend))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FarmReg);
