import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock,
    Button,

} from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import '../res/scss/form.css';

import { SolidLoadingAnimation, TransparentLoadingAnimation } from '../utils/loadingAnimation.js'

import { getAppoinmentInfo, updateAppoinmentInfo, addAppoinmentInfo } from '../actions/appoinment_actions';
import { connect } from 'react-redux';

class Form1 extends Component {
    constructor(props) {
        super(props)

        var isThisEditVisit = false;
        if (this.props.match.params.id) {
            isThisEditVisit = true;
        }

        this.state = {
            employeeName: "",
            date: moment(),
            designation: "",
            address: "",
            addDate: moment(),
            appDate: moment(),
            intDate: moment(),
            effDate: moment(),
            basicPay: "",
            recomendationOf: "",
            typeOfEmployment: "",

            isThisEdit: isThisEditVisit,
            isThisFirstTime: true,
            isSubmitted: false
        };
    }

    componentWillMount() {
        if (this.state.isThisEdit) {
            console.log("id is there>>" + this.state.isThisEdit);
            this.props.getAppoinmentInfo({ id: this.props.match.params.id });
        } else {
            console.log("id is not there>>" + this.state.isThisEdit);
        }
    }

    handleChange(from, date) {
        console.log("from date", from, " ", date);
        this.setState({
            [from]: date
        });
    }

    validateAllFields() {
        var statusToReturn = true;
        var idsOfFieldsToValidate = ["employeeName", "designation", "address"]
        for (var i = 0; i < idsOfFieldsToValidate.length; i++) {
            var currentElement = document.getElementById(idsOfFieldsToValidate[i]);
            if (currentElement.value != "" && currentElement.value != "select") {//|| currentElement.value != "select"
                console.log("ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border:dummy")
            } else {
                console.log("not ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border: 1px solid #d44545")
                statusToReturn = false;
            }
        }
        if (this.validateNumberCheck()) {
            return statusToReturn;
        } else {
            return false;
        }
    }

    validateNumberCheck() {
        var regExForNumericalValue = /^[0-9]+$/;
        var statusToReturn = true;
        var idsOfFieldsToValidate = ["basicPay"]
        for (var i = 0; i < idsOfFieldsToValidate.length; i++) {
            var currentElement = document.getElementById(idsOfFieldsToValidate[i]);
            if (regExForNumericalValue.test(currentElement.value)) {
                console.log("ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border:dummy")
            } else {
                console.log("not ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border: 1px solid #d44545")
                statusToReturn = false;
            }
        }
        return statusToReturn;
    }


    handleUserInputs(type, newValue) {
        this.state[type] = newValue;
        this.setState({ owner: "" });
    }


    submitButtonClicked() {
        if (this.validateAllFields()) {
            this.setState({ isSubmitted: true });
            var dataToSend = {
                name: this.state.employeeName,
                date: this.state.date.format("DD-MM-YYYY"),
                designation: this.state.designation,
                address: this.state.address,
                advt_date: this.state.addDate.format("DD-MM-YYYY"),
                application_date: this.state.appDate.format("DD-MM-YYYY"),
                interview_date: this.state.intDate.format("DD-MM-YYYY"),
                effect_from_date: this.state.effDate.format("DD-MM-YYYY"),
                salary: this.state.basicPay,
                recommendation: this.state.recomendationOf,
                type: this.state.typeOfEmployment
            }
            console.log("dataToSend is >>", dataToSend);
            if (this.state.isThisEdit) {
                console.log("edit submitted dataToSend >>", dataToSend);
                this.props.updateAppoinmentInfo(this.props.match.params.id, dataToSend);
            } else {
                console.log("add submitted dataToSend >>", dataToSend);
                this.props.addAppoinmentInfo(dataToSend);
            }
        } else {
            alert("please fill forms");
        }
    }

    render() {
        if (this.props.getAppoinmentResponse.success == true && this.state.isThisFirstTime) {
            this.state.isThisFirstTime = false
            var responseObject = this.props.getAppoinmentResponse.responseData.data.rows[0];
            console.log(" getAppoinmentResponse ", responseObject);

            this.state.employeeName = responseObject.name;
            this.state.date = moment(responseObject.date);
            this.state.designation = responseObject.designation;
            this.state.address = responseObject.address;
            this.state.addDate = moment(responseObject.advt_date);
            this.state.appDate = moment(responseObject.application_date);
            this.state.intDate = moment(responseObject.interview_date);
            this.state.effDate = moment(responseObject.effect_from_date);
            this.state.basicPay = responseObject.salary;
            this.state.recomendationOf = responseObject.recommendation;
            this.state.typeOfEmployment = responseObject.type;
        }


        if (this.props.addAppoinmentResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("addAppoinmentResponse success");
        }

        if (this.props.updateAppoinmentResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("updateAppoinmentResponse success");
        }



        return (
            <div className="formWrapper">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="col-sm-12">
                            <div className="form-header">
                                <h3>APPOINTMENT</h3>
                                <span className="subhead">Appointment Information</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <form className="info-form">
                            <div className="col-sm-4">
                                <label>Employee Name</label>
                                <input id="employeeName" required={true} value={this.state.employeeName} type="text" className="form-control" placeholder="Employee Name" onChange={(event) => this.handleUserInputs("employeeName", event.target.value)} />
                            </div>
                            <div className="col-sm-4">
                                <label>Date</label>
                                <DatePicker className="form-control"
                                    selected={this.state.date}
                                    onChange={this.handleChange.bind(this, 'date')}
                                />
                            </div>
                            <div className="col-sm-4">
                                <label>Designation</label>
                                <select id="designation" value={this.state.designation} className="form-control" onChange={(event) => this.handleUserInputs("designation", event.target.value)} >
                                    <option value="select">Select</option>
                                    <option value="designation1">Designation1</option>
                                    <option value="MD">MD</option>
                                </select>
                            </div>
                            <div className="col-sm-8">
                                <label>Employee Address</label>
                                <input id="address" value={this.state.address} type="text" className="form-control" placeholder="Employee Address" onChange={(event) => this.handleUserInputs("address", event.target.value)} />
                            </div>
                            <div className="col-sm-4">
                                <label>Advertisement Date</label>
                                <DatePicker className="form-control"
                                    selected={this.state.addDate}
                                    onChange={this.handleChange.bind(this, "addDate")}
                                />
                            </div>
                            <div className="col-sm-4">
                                <label>Application Date</label>
                                <DatePicker className="form-control"
                                    selected={this.state.appDate}
                                    onChange={this.handleChange.bind(this, "appDate")}
                                />
                            </div>
                            <div className="col-sm-4">
                                <label>InterView Held On</label>
                                <DatePicker className="form-control"
                                    selected={this.state.intDate}
                                    onChange={this.handleChange.bind(this, "intDate")}
                                />
                            </div>
                            <div className="col-sm-4">
                                <label>Effect From Date</label>
                                <DatePicker className="form-control"
                                    id="effect"
                                    selected={this.state.effDate}
                                    onChange={this.handleChange.bind(this, 'effDate')}
                                />
                            </div>
                            <div className="col-sm-4">
                                <label>Basic Pay</label>
                                <input id="basicPay" value={this.state.basicPay} type="text" className="form-control" placeholder="Basic Pay" onChange={(event) => this.handleUserInputs("basicPay", event.target.value)} />
                            </div>
                            <div className="col-sm-4">
                                <label>Recommendation Of</label>
                                <input value={this.state.recomendationOf} type="text" className="form-control" placeholder="Recommendation Of" onChange={(event) => this.handleUserInputs("recomendationOf", event.target.value)} />
                            </div>
                            <div className="col-sm-4">
                                <label>Type Of Employment</label>
                                <select value={this.state.typeOfEmployment} className="form-control" onChange={(event) => this.handleUserInputs("typeOfEmployment", event.target.value)} >
                                    <option value="select">Select</option>
                                    <option value="type1">Type1</option>
                                    <option value="jj">jj</option>
                                </select>
                            </div>
                            <div className="col-sm-12" style={{ justifyContent: "center", flex: 1, display: "flex", flexDirection: "column", alignItems: "center", padding: 40 }}>
                                <Button type="submit" bsStyle="primary" form="form" onClick={() => this.submitButtonClicked()} style={{ width: 100, color: "#fff" }}>Submit</Button>
                            </div>
                        </form>
                    </div>
                </div>
                <TransparentLoadingAnimation isVisible={this.props.getAppoinmentResponse.loading || this.props.addAppoinmentResponse.loading || this.props.updateAppoinmentResponse.loading} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    getAppoinmentResponse: state.getAppoinmentResponse,
    addAppoinmentResponse: state.addAppoinmentResponse,
    updateAppoinmentResponse: state.updateAppoinmentResponse
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        getAppoinmentInfo: (dataToSend) => {
            dispatch(getAppoinmentInfo(dataToSend))
        },
        addAppoinmentInfo: (dataToSend) => {
            dispatch(addAppoinmentInfo(dataToSend))
        },
        updateAppoinmentInfo: (id, dataToSend) => {
            dispatch(updateAppoinmentInfo(id, dataToSend))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Form1);

