import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import '../res/scss/form.css';

import { submitDischargeInfo } from '../actions/discharge_actions';
import { SolidLoadingAnimation, TransparentLoadingAnimation } from '../utils/loadingAnimation.js'

import { connect } from 'react-redux';

import {
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock,
    Button

} from 'react-bootstrap';

class DischargeForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isSubmitted: false,
            date: moment(),
            appDate: moment(),
            addDate: moment(),
            intDate: moment(),
            effDate: moment(),
            userSelectedValues: {
                employeeId: '',
                exitType: 'select',
                exitReason: '',
            }
        };
    }

    handleChange(from, date) {
        console.log("date >> ", date.format("DD-MM-YYYY HH:mm:ss"));
        this.setState({
            [from]: date
        });
    }

    handleUserInputs(type, newValue) {
        if (type == "employeeId") {
            this.state.userSelectedValues.employeeId = newValue;
        } else if (type == "exitType") {
            this.state.userSelectedValues.exitType = newValue;
        } else if (type == "exitReason") {
            this.state.userSelectedValues.exitReason = newValue;
        }
        this.setState({ owner: "" });
    }

    validateAllFields() {
        var statusToReturn = true;
        var idsOfFieldsToValidate = ["exitType", "employeeId", "exitReason"]
        for (var i = 0; i < idsOfFieldsToValidate.length; i++) {
            var currentElement = document.getElementById(idsOfFieldsToValidate[i]);
            if (currentElement.value != "" && currentElement.value != "select") {//|| currentElement.value != "select"
                console.log("ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border:dummy")
            } else {
                console.log("not ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border: 1px solid #d44545")
                statusToReturn = false;
            }
        }
        return statusToReturn;
    }


    submitButtonClicked() {
        if (this.validateAllFields()) {
            var dataToSend = {
                exittype: this.state.userSelectedValues.exitType,
                exitreason: this.state.userSelectedValues.exitReason,
                exitdate: this.state.date.format("DD-MM-YYYY HH:mm:ss")
            }
            var employeeId = this.state.userSelectedValues.employeeId;
            this.setState({ isSubmitted: true })
            console.log("dataToSend is >>", dataToSend);
            this.props.submitDischargeInfo(employeeId, dataToSend)
        } else {
            alert("please fill fields");
        }

    }

    render() {
        if (this.props.dischargeSubmitResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("submitted successfully");
        }

        return (
            <div className="formWrapper">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="col-sm-12">
                            <div className="form-header" style={{ borderRadius: 2 }}>
                                <h3 style={{ padding: 5 }}>Discharge</h3>
                                <span style={{ padding: 5 }} className="subhead">Exit Information</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12" style={{ padding: 25 }}>
                        <form className="info-form">
                            <div className="col-sm-4">
                                <label>Employee Id</label>
                                <input id="employeeId" value={this.state.userSelectedValues.employeeId} type="text" className="form-control" placeholder="Employee Id" onChange={(event) => this.handleUserInputs("employeeId", event.target.value)} />
                            </div>
                        </form>
                    </div>
                </div>
                {/* <hr /> */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="col-sm-12">
                            <div className="form-header">
                                <h3></h3>
                                <span style={{ padding: 5, borderRadius: 2 }} className="subhead">Exit Description</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12" style={{ padding: 25 }}>
                        <form className="info-form">
                            <div className="col-sm-4">
                                <label>Exit Date</label>
                                <DatePicker className="form-control"
                                    selected={this.state.date}
                                    onChange={this.handleChange.bind(this, 'date')}
                                />
                            </div>
                            <div className="col-sm-4">
                                <label>Type Of Exit</label>
                                <select id="exitType" value={this.state.userSelectedValues.exitType} className="form-control" onChange={(event) => this.handleUserInputs("exitType", event.target.value)}>
                                    <option value="select">Select</option>
                                    <option value="type1">type1</option>
                                    <option value="type2">type2</option>
                                </select>
                            </div>
                            <div className="col-sm-12" style={{ paddingTop: 30 }}>
                                <label>Exit Reason</label>
                                <textarea id="exitReason" value={this.state.userSelectedValues.exitReason} className="form-control" placeholder="Exit Reason" onChange={(event) => this.handleUserInputs("exitReason", event.target.value)} />
                            </div>
                        </form>
                    </div>
                </div>
                <div className="col-sm-12" style={{ justifyContent: "center", flex: 1, display: "flex", flexDirection: "column", alignItems: "center", padding: 40 }}>
                    <Button bsStyle="primary" form="form" onClick={() => this.submitButtonClicked()} style={{ width: 100, color: "#fff" }}>Submit</Button>
                </div>
                <TransparentLoadingAnimation isVisible={this.props.dischargeSubmitResponse.loading} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    dischargeSubmitResponse: state.dischargeSubmitResponse
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        submitDischargeInfo: (dataToSend) => {
            dispatch(submitDischargeInfo(dataToSend))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DischargeForm);
