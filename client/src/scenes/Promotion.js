import React, { Component } from 'react';
import {
  FormGroup,
  FormControl,
  ControlLabel,
  HelpBlock,
  Button,
  Panel,
  PageHeader,
  Grid,
  Row,
  Col
} from 'react-bootstrap';
import { Column, Table, AutoSizer } from 'react-virtualized';
import { connect } from 'react-redux';
import { getAcademicSessionReport,getAdmissionRegistrationReport,promoteTheStudent } from '../actions/student_actions.js'


import { SolidLoadingAnimation, TransparentLoadingAnimation } from '../utils/loadingAnimation.js'

var Datetime = require('react-datetime');

class PromoteStudentForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      key: 1,
      switched: [false, false, false],
      formData: {
        regno: '',
        join_date: '',
        academic_year: '',
        division: '',
        class: '',
        semester: '',
        bus: '',
        hostel: '',
        mess: ''
      },
      isSubmitClicked: false,
      isSearchClicked:false,
      isVisibleTransparentLoading: false,
      availabeValuesOfDropdown: {
        academicYear: [],
        promotionAcademicYear: [],
        class: [],
        promotionClass: [],
        semester: [],
        promotionSemester: [],
        division: [],
        promotionDivision: []
      },
      userSelectedValues: {
        academicYear: 'select',
        nextAcademicYear: 'select',
        class: 'select',
        nextClass: 'select',
        semester: 'select',
        nextSemester: 'select',
        division: 'select',
        nextDivision: 'select'
      },
      promoteArray:[]
    };
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSwitch = this.handleSwitch.bind(this);
  }

  componentWillMount() {
    this.props.getAcademicSessionReport();
  }
  componentDidMount(){
  }

  blankInputValidation(label) {
    if (this.state.isSubmitClicked) {
      const length = this.state.formData[label].length;
      if (length === 0) return 'error';
    }
    return null;
  }

  saveForm() {
    this.setState({ isSubmitClicked: true });
    if (this.validateAllFields()) {
      var nextAcademicYear =this.state.userSelectedValues.nextAcademicYear !="select" ? this.state.userSelectedValues.nextAcademicYear:"";
      var nextClass =this.state.userSelectedValues.nextClass !="select" ? this.state.userSelectedValues.nextClass:""; 
      var nextSemester =this.state.userSelectedValues.nextSemester !="select" ? this.state.userSelectedValues.nextSemester:""; 
      var nextDivision =this.state.userSelectedValues.nextDivision !="select" ? this.state.userSelectedValues.nextDivision:""; 
      var finalPromoteArrayToSend=[];
      var tempPromoteArray=this.state.promoteArray;

      for(var i=0;i<tempPromoteArray.length;i++){
        if(tempPromoteArray[i].needToSend == true){
          var tempObject={};
          tempObject["admissionRegistrationMasterId"]=tempPromoteArray[i].admissionRegistrationMasterId;
          tempObject["academicSessionId"]=tempPromoteArray[i].academicSessionId;
          tempObject["academicYear"]=nextAcademicYear;
          tempObject["class"]=nextClass;
          tempObject["semester"]=nextSemester;
          tempObject["division"]=nextDivision;
          tempObject["bus"]=tempPromoteArray[i].bus;
          tempObject["hostel"]=tempPromoteArray[i].hostel;
          tempObject["mess"]=tempPromoteArray[i].mess;
          finalPromoteArrayToSend.push(tempObject);
        }
      }
      
      var dataToSend = {
          promoteArray:finalPromoteArrayToSend,
      }
      console.log("dataToSend is >>", dataToSend);
      this.props.promoteTheStudent(dataToSend)
    } else {
      alert("please fill fields");
    }
  }

  validateAllFields() {
    var statusToReturn = true;
    var idsOfFieldsToValidate = ["nextAcademicYear", "nextClass"]
    for (var i = 0; i < idsOfFieldsToValidate.length; i++) {
      var currentElement = document.getElementById(idsOfFieldsToValidate[i]);
      if (currentElement.value != "" && currentElement.value != "select") {//|| currentElement.value != "select"
        console.log("ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
        currentElement.setAttribute("style", "border:dummy")
      } else {
        console.log("not ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
        currentElement.setAttribute("style", "border: 1px solid #d44545")
        statusToReturn = false;
      }
    }
    return statusToReturn;
  }

  validateAllFieldsForSearch(){
    var statusToReturn = true;
    var idsOfFieldsToValidate = ["academicYear", "class"]
    for (var i = 0; i < idsOfFieldsToValidate.length; i++) {
      var currentElement = document.getElementById(idsOfFieldsToValidate[i]);
      if (currentElement.value != "" && currentElement.value != "select") {//|| currentElement.value != "select"
        console.log("ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
        currentElement.setAttribute("style", "border:dummy")
      } else {
        console.log("not ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
        currentElement.setAttribute("style", "border: 1px solid #d44545")
        statusToReturn = false;
      }
    }
    return statusToReturn;
  }

  handleChange(label, val) {
    console.log(val);
    this.setState(state => {
      state.formData[label] = val;
      return state;
    }, () => {
      console.log(this.state);
    });
  }

  handleSwitch(i) {
    this.setState(state => {
      state.switched[i] = !state.switched[i];
      return state;
    });
  }

  handleSelect(type, newValue) {
    var tempUserSelectedValues = this.state.userSelectedValues;
    if (type == "academicYear") {
      var tempClasses = [];
      var responseData = this.props.academicSessionReport.responseData;
      var rows = responseData.result.rows;
      for (var i = 0; i < rows.length; i++) {
        var currentRow = rows[i];
        if (currentRow.year == newValue) {
          var currentAcademicSession = currentRow.academic_sessions;
          for (var j = 0; j < currentAcademicSession.length; j++) {
            tempClasses.push(currentAcademicSession[j].class);
          }
        }
      }
      this.state.availabeValuesOfDropdown.class = tempClasses;
      tempUserSelectedValues.academicYear = newValue;
      tempUserSelectedValues.class = "select";
      tempUserSelectedValues.semester = "select";
      tempUserSelectedValues.division = "select";
      this.setState({ userSelectedValues: tempUserSelectedValues });
    } else if (type == "class") {
      var tempSemesters = [];
      var tempDivisions = [];
      var responseData = this.props.academicSessionReport.responseData;
      var rows = responseData.result.rows;
      for (var i = 0; i < rows.length; i++) {
        var currentRow = rows[i];
        if (currentRow.year == this.state.userSelectedValues.academicYear) {
          var currentAcademicSession = currentRow.academic_sessions;
          for (var j = 0; j < currentAcademicSession.length; j++) {
            if (currentAcademicSession[j].class == newValue) {
              tempSemesters = currentAcademicSession[j].semester.split(",");
              tempDivisions = currentAcademicSession[j].division.split(",");
            }
          }
        }
      }
      this.state.availabeValuesOfDropdown.semester = tempSemesters;
      this.state.availabeValuesOfDropdown.division = tempDivisions;
      tempUserSelectedValues.class = newValue;
      tempUserSelectedValues.semester = "select";
      tempUserSelectedValues.division = "select";
      this.setState({ userSelectedValues: tempUserSelectedValues });
    } else if (type == "semester") {
      tempUserSelectedValues.semester = newValue;
      this.setState({ userSelectedValues: tempUserSelectedValues });
    } else if (type == "division") {
      tempUserSelectedValues.division = newValue;
      this.setState({ userSelectedValues: tempUserSelectedValues });
    }
  }


  handlePromotionSelect(type, newValue) {
    var tempUserSelectedValues = this.state.userSelectedValues;
    if (type == "academicYear") {
      var tempClasses = [];
      var responseData = this.props.academicSessionReport.responseData;
      var rows = responseData.result.rows;
      for (var i = 0; i < rows.length; i++) {
        var currentRow = rows[i];
        if (currentRow.year == newValue) {
          var currentAcademicSession = currentRow.academic_sessions;
          for (var j = 0; j < currentAcademicSession.length; j++) {
            tempClasses.push(currentAcademicSession[j].class);
          }
        }
      }
      this.state.availabeValuesOfDropdown.promotionClass = tempClasses;
      tempUserSelectedValues.nextAcademicYear = newValue;
      tempUserSelectedValues.nextClass = "select";
      tempUserSelectedValues.nextSemester = "select";
      tempUserSelectedValues.nextDivision = "select";
    } else if (type == "class") {
      this.state.userSelectedValues.nextClass = newValue;
      var tempSemesters = [];
      var tempDivisions = [];
      var responseData = this.props.academicSessionReport.responseData;
      var rows = responseData.result.rows;
      for (var i = 0; i < rows.length; i++) {
        var currentRow = rows[i];
        if (currentRow.year == this.state.userSelectedValues.academicYear) {
          var currentAcademicSession = currentRow.academic_sessions;
          for (var j = 0; j < currentAcademicSession.length; j++) {
            if (currentAcademicSession[j].class == newValue) {
              tempSemesters = currentAcademicSession[j].semester.split(",");
              tempDivisions = currentAcademicSession[j].division.split(",");
            }
          }
        }
      }
      this.state.availabeValuesOfDropdown.promotionSemester = tempSemesters;
      this.state.availabeValuesOfDropdown.promotionDivision = tempDivisions;
      tempUserSelectedValues.nextSemester = "select";
      tempUserSelectedValues.nextDivision = "select";
    } else if (type == "semester") {
      this.state.userSelectedValues.nextSemester = newValue;
    } else if (type == "division") {
      this.state.userSelectedValues.nextDivision = newValue;
    }
    this.setState({ owner: "" })
  }

  searchButtonCLicked() {
    if (this.validateAllFieldsForSearch()) {
      this.state.isSearchClicked=true;
      this.state.promoteArray=[];
      var dataToSend = {
        academicYear: this.state.userSelectedValues.academicYear != "select" ? this.state.userSelectedValues.academicYear : "-",
        class: this.state.userSelectedValues.class != "select" ? this.state.userSelectedValues.class : "-",
        semester: this.state.userSelectedValues.semester != "select" ? this.state.userSelectedValues.semester : "-",
        division: this.state.userSelectedValues.division != "select" ? this.state.userSelectedValues.division : "-"
      }
      console.log("dataToSend is >>", dataToSend);
      this.props.getAdmissionRegistrationReport(dataToSend);
    } else {
      alert("please fill fields")
    }
  }

  checkBoxChangeHandler(type,newValue,regNo){
    console.log("type newValue >>"+type+" ",newValue+" "+regNo);
    var tempPromoteArray=this.state.promoteArray;
    if(type=="allRow"){
      this.updatePromoteArrayForSelectAllCheckboxes("allRow",newValue)
      if(newValue){
        var allRows=document.getElementsByClassName("rowSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=true;
        }
      }else{
        var allRows=document.getElementsByClassName("rowSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=false;
        }
      }
    }else if(type=="allBus"){
      this.updatePromoteArrayForSelectAllCheckboxes("allBus",newValue)
      if(newValue){
        var allRows=document.getElementsByClassName("busSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=true;
        }
      }else{
        var allRows=document.getElementsByClassName("busSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=false;
        }
      }
    }else if(type=="allHostel"){
      this.updatePromoteArrayForSelectAllCheckboxes("allHostel",newValue)
      if(newValue){
        var allRows=document.getElementsByClassName("hostelSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=true;
        }
      }else{
        var allRows=document.getElementsByClassName("hostelSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=false;
        }
      }
    }else if(type=="allMess"){
      this.updatePromoteArrayForSelectAllCheckboxes("allMess",newValue)
      if(newValue){
        var allRows=document.getElementsByClassName("messSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=true;
        }
      }else{
        var allRows=document.getElementsByClassName("messSelectorCheckbox")
        for(var i=0;i<allRows.length;i++){
          allRows[i].checked=false;
        }
      }
    }else if(type=="singleRow"){
      console.log("u clicked singleRow");
      for(var i=0;i<tempPromoteArray.length;i++){
        if(tempPromoteArray[i].regno == regNo){
          tempPromoteArray[i].needToSend = newValue;
        }
      }
    }else if(type=="singleBus"){
      console.log("u clicked singleBus");
      for(var i=0;i<tempPromoteArray.length;i++){
        if(tempPromoteArray[i].regno == regNo){
          tempPromoteArray[i].bus = newValue;
        }
      }
    }else if(type=="singleHostel"){
      console.log("u clicked singleHostel");
      for(var i=0;i<tempPromoteArray.length;i++){
        if(tempPromoteArray[i].regno == regNo){
          tempPromoteArray[i].hostel = newValue;
        }
      }
    }else if(type=="singleMess"){
      console.log("u clicked singleMess");
      for(var i=0;i<tempPromoteArray.length;i++){
        if(tempPromoteArray[i].regno == regNo){
          tempPromoteArray[i].mess = newValue;
        }
      }
    }
    this.state.promoteArray=tempPromoteArray;
    console.log("this.state.promoteArray >>",this.state.promoteArray);
  }

  updatePromoteArrayForSelectAllCheckboxes(type,newValue){
    var tempPromoteArray=this.state.promoteArray;
    if(type=="allRow"){
      for(var i=0;i<tempPromoteArray.length;i++){
        tempPromoteArray[i].needToSend = newValue;
      }
    }else if(type=="allBus"){
      for(var i=0;i<tempPromoteArray.length;i++){
        tempPromoteArray[i].bus = newValue;
      }
    }
    else if(type=="allHostel"){
      for(var i=0;i<tempPromoteArray.length;i++){
        tempPromoteArray[i].hostel = newValue;
      }
    }
    else if(type=="allMess"){
      for(var i=0;i<tempPromoteArray.length;i++){
        tempPromoteArray[i].mess = newValue;
      }
    }
  }

  renderTable() {
    var list = [];
    if (this.props.admissionRegistrationReport.success == true) {
      list = this.props.admissionRegistrationReport.responseData.result.rows;
    }
    if (this.props.admissionRegistrationReport.success == true && this.state.isSearchClicked) {
      this.state.isSearchClicked=false;
      for(var i=0;i<list.length;i++){
        var dataToPush={};
        var currentRow=list[i];
        var tempAdmissionRegistrations={};
        if(currentRow.admission_registrations && currentRow.admission_registrations[0]){
          tempAdmissionRegistrations=currentRow.admission_registrations[0];
        }
        dataToPush["needToSend"]=false;
        dataToPush["regno"]=currentRow.regno;
        dataToPush["admissionRegistrationMasterId"]=tempAdmissionRegistrations.admissionRegistrationMasterId;
        dataToPush["academicSessionId"]=tempAdmissionRegistrations.academicSessionId;
        dataToPush["bus"]=tempAdmissionRegistrations.bus;
        dataToPush["hostel"]=tempAdmissionRegistrations.hostel;
        dataToPush["mess"]=tempAdmissionRegistrations.mess;
        this.state.promoteArray.push(dataToPush);
      }
      console.log("this.state.promoteArray >>",this.state.promoteArray);
    }
    return (
      <AutoSizer disableHeight>
        {({ width }) => (
          <Table
            width={width}
            height={600}
            autoHeight={true}
            headerHeight={20}
            rowHeight={30}
            rowCount={list.length}
            rowGetter={({ index }) => list[index]}
            noRowsRenderer={() => <div style={{ paddingTop: 20, textAlign: "center" }}>No data found</div>}
            // onRowClick={() => { alert("u clicked row") }}
            style={{}}
            gridStyle={{}}
          >
            <Column
              width={50}
              headerRenderer={(data) => {
                return (<input type="checkbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("allRow", event.target.checked)}/>)
              }}
              dataKey=''
              cellRenderer={(data) => {
                return (<input type="checkbox" className="rowSelectorCheckbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("singleRow", event.target.checked,data.rowData.regno)}/>)
              }}
            />
            <Column
              width={200}
              label='RegNo'
              dataKey='regno'
              headerStyle={{ textAlign: "center" }}
              style={{ textAlign: "center" }}
            />
            <Column
              width={200}
              label='Name'
              dataKey='student_name'
              headerStyle={{ textAlign: "center" }}
              style={{ textAlign: "center" }}
            />
            <Column
              width={200}
              headerRenderer={(data) => {
                return (<div style={{display:"flex",justifyContent: "center"}}><input type="checkbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("allBus",event.target.checked)}/><span style={{paddingLeft: 5}}>Bus</span></div>)
              }}
              dataKey='bus'
              headerStyle={{ textAlign: "center" }}
              style={{ textAlign: "center" }}
              cellRenderer={(data) => {
                var tempData =false;
                if(data.rowData.admission_registrations && data.rowData.admission_registrations[0]){
                  tempData=data.rowData.admission_registrations[0][data.dataKey];
                }
                if (tempData == true) {
                  return (<input type="checkbox" className="busSelectorCheckbox" defaultChecked={true} onChange={(event)=>this.checkBoxChangeHandler("singleBus", event.target.checked,data.rowData.regno)}/>)
                } else {
                  return (<input type="checkbox" className="busSelectorCheckbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("singleBus", event.target.checked,data.rowData.regno)}/>)
                }
              }}
            />
            <Column
              width={200}
              headerRenderer={(data) => {
                return (<div style={{display:"flex",justifyContent: "center"}}><input type="checkbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("allHostel",event.target.checked)}/><span style={{paddingLeft: 5}}>Hostel</span></div>)
              }}
              dataKey='hostel'
              headerStyle={{ textAlign: "center" }}
              style={{ textAlign: "center" }}
              cellRenderer={(data) => {
                var tempData =false;
                if(data.rowData.admission_registrations && data.rowData.admission_registrations[0]){
                  tempData=data.rowData.admission_registrations[0][data.dataKey];
                }
                if (tempData == true) {
                  return (<input type="checkbox" className="hostelSelectorCheckbox" defaultChecked={true} onChange={(event)=>this.checkBoxChangeHandler("singleHostel", event.target.checked,data.rowData.regno)}/>)
                } else {
                  return (<input type="checkbox" className="hostelSelectorCheckbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("singleHostel", event.target.checked,data.rowData.regno)}/>)
                }
              }}
            />
            <Column
              width={200}
              headerRenderer={(data) => {
                return (<div style={{display:"flex",justifyContent: "center"}}><input type="checkbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("allMess",event.target.checked)}/><span style={{paddingLeft: 5}}>Mess</span></div>)
              }}
              dataKey='mess'
              headerStyle={{ textAlign: "center" }}
              style={{ textAlign: "center" }}
              cellRenderer={(data) => {
                var tempData =false;
                if(data.rowData.admission_registrations && data.rowData.admission_registrations[0]){
                  tempData=data.rowData.admission_registrations[0][data.dataKey];
                }
                if (tempData == true) {
                  return (<input type="checkbox" className="messSelectorCheckbox" defaultChecked={true} onChange={(event)=>this.checkBoxChangeHandler("singleMess", event.target.checked,data.rowData.regno)}/>)
                } else {
                  return (<input type="checkbox" className="messSelectorCheckbox" defaultChecked={false} onChange={(event)=>this.checkBoxChangeHandler("singleMess", event.target.checked,data.rowData.regno)}/>)
                }
              }}
            />
          </Table>
        )}
      </AutoSizer>
    );
  }




  render() {
    if (this.props.academicSessionReport.success == true) {
      var responseData = this.props.academicSessionReport.responseData;
      var rows = responseData.result.rows;
      var tempAcademicYears = [];
      for (var i = 0; i < rows.length; i++) {
        tempAcademicYears.push(rows[i].year)
      }
      this.state.availabeValuesOfDropdown.academicYear = tempAcademicYears;
      this.state.availabeValuesOfDropdown.promotionAcademicYear = tempAcademicYears;
    }

    if (this.props.promoteTheStudentResponse.success == true && this.state.isSubmitClicked == true) {
      alert("successfully promoted");
      this.setState({ isSubmitClicked: false });
    }

    return (
      <div>
        <form method="post" id="form">
          <FormGroup>
            <Grid className="registration-form-container">
              <Row>
                <Col xs={12} md={11}>
                  <PageHeader><small>Promote Form </small></PageHeader>
                </Col>
              </Row>
              <Row className="show-grid">
                <Col xs={12} md={11}>
                  <Panel>
                    <Panel.Body>
                      <FormGroup>
                        <Panel>
                          <Panel.Heading>
                            <Panel.Title componentClass="h3">Promote Student</Panel.Title>
                          </Panel.Heading>
                          <Panel.Body>
                            <Grid className="registration-form-container">
                              <Row className="show-grid">
                                <Col xs={5} md={4} lg={3}>
                                  <ControlLabel>Academic Year</ControlLabel>
                                  <FormControl id="academicYear" value={this.state.userSelectedValues.academicYear} componentClass="select" placeholder="select" onChange={(event) => this.handleSelect("academicYear", event.target.value)}>
                                    <option value="select">select</option>
                                    {this.state.availabeValuesOfDropdown.academicYear.map(temp =>
                                      <option value={temp}>{temp}</option>
                                    )}
                                  </FormControl>
                                </Col>
                                <Col xs={5} md={4} lg={3}>
                                  <ControlLabel>Class</ControlLabel>
                                  <FormControl id="class" value={this.state.userSelectedValues.class} componentClass="select" placeholder="select" onChange={(event) => this.handleSelect("class", event.target.value)}>
                                    <option value="select">select</option>
                                    {this.state.availabeValuesOfDropdown.class.map(temp =>
                                      <option value={temp}>{temp}</option>
                                    )}
                                  </FormControl>
                                </Col>
                                <Col xs={5} md={4} lg={3}>
                                  <div className="form-admission">
                                    <div className="layout-row">
                                      <ControlLabel>Semester</ControlLabel>
                                      <FormControl id="semester" value={this.state.userSelectedValues.semester} className="form-admission-input" componentClass="select" placeholder="select" onChange={(event) => this.handleSelect("semester", event.target.value)}>
                                        <option value="select">select</option>
                                        {this.state.availabeValuesOfDropdown.semester.map(temp =>
                                          <option value={temp}>{temp}</option>
                                        )}
                                      </FormControl>
                                    </div>
                                    <div className="layout-row" style={{ paddingLeft: 5 }}>
                                      <ControlLabel>Division</ControlLabel>
                                      <FormControl id="division" value={this.state.userSelectedValues.division} className="form-admission-input" componentClass="select" placeholder="select" onChange={(event) => this.handleSelect("division", event.target.value)}>
                                        <option value="select">select</option>
                                        {this.state.availabeValuesOfDropdown.division.map(temp =>
                                          <option value={temp}>{temp}</option>
                                        )}
                                      </FormControl>
                                    </div>
                                    <div style={{ display: "flex", alignItems: "flex-end", paddingLeft: 5 }}>
                                      <Button bsStyle="primary" form="form" onClick={() => this.searchButtonCLicked()}>Search</Button>
                                    </div>
                                  </div>
                                </Col>
                              </Row>
                            </Grid>
                          </Panel.Body>
                        </Panel>
                        <Panel>
                          <Panel.Body>
                            {this.renderTable()}
                          </Panel.Body>
                        </Panel>
                        <Panel>
                          <Panel.Body>
                            <Row className="show-grid">
                              <Col md={3}>
                                <ControlLabel>Academic Year</ControlLabel>
                                <FormControl id="nextAcademicYear"  value={this.state.userSelectedValues.nextAcademicYear} componentClass="select" placeholder="select" onChange={(event) => this.handlePromotionSelect("academicYear", event.target.value)} >
                                  <option value="select">select</option>
                                  {this.state.availabeValuesOfDropdown.promotionAcademicYear.map(temp =>
                                    <option value={temp}>{temp}</option>
                                  )}
                                </FormControl>
                              </Col>
                              <Col md={3}>
                                <ControlLabel>Class</ControlLabel>
                                <FormControl id="nextClass" value={this.state.userSelectedValues.nextClass} componentClass="select" placeholder="select" onChange={(event) => this.handlePromotionSelect("class", event.target.value)}>
                                  <option value="select">select</option>
                                  {this.state.availabeValuesOfDropdown.promotionClass.map(temp =>
                                    <option value={temp}>{temp}</option>
                                  )}
                                </FormControl>
                              </Col>
                              <Col md={3} >
                                <ControlLabel>Semester</ControlLabel>
                                <FormControl value={this.state.userSelectedValues.nextSemester} componentClass="select" placeholder="select" onChange={(event) => this.handlePromotionSelect("semester", event.target.value)}>
                                  <option value="select">select</option>
                                  {this.state.availabeValuesOfDropdown.promotionSemester.map(temp =>
                                    <option value={temp}>{temp}</option>
                                  )}
                                </FormControl>
                              </Col>

                              <Col md={3} >
                                <ControlLabel>Division</ControlLabel>
                                <FormControl value={this.state.userSelectedValues.nextDivision} componentClass="select" placeholder="select" onChange={(event) => this.handlePromotionSelect("division", event.target.value)}>
                                  <option value="select">select</option>
                                  {this.state.availabeValuesOfDropdown.promotionDivision.map(temp =>
                                    <option value={temp}>{temp}</option>
                                  )}
                                </FormControl>
                              </Col>
                            </Row>
                          </Panel.Body>
                        </Panel>
                        <div className="form-register-btn layout-column" style={{ textAlign: "center" }}>
                          <Button bsStyle="primary" form="form" onClick={() => { this.saveForm() }}>PROMOTE</Button>
                        </div>
                      </FormGroup>
                    </Panel.Body>
                  </Panel>
                </Col>
              </Row>
            </Grid>
          </FormGroup>
        </form>
        <TransparentLoadingAnimation isVisible={this.props.academicSessionReport.loading || this.props.admissionRegistrationReport.loading || this.props.promoteTheStudentResponse.loading} />
      </div>
    );
  }
}

class FieldGroup extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    console.log(e.target.value);
    this.props.sendChange(e.target.value);
  }

  validateValue(value) {
    if (this.props.isSubmitClicked) {
      const length = this.props.value.length;
      console.log(length);
      if (length === 0) return 'error';
    }
    return null;
  }

  render() {
    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validateValue(this.props.value)}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          className={this.props.className}
          type={this.props.type}
          label={this.props.label}
          placeholder={this.props.placeholder}
          value={this.props.value}
          onChange={(e) => { this.handleChange(e) }} />
        {this.props.help && <HelpBlock>{this.props.help}</HelpBlock>}
      </FormGroup>
    );
  }

};

const Switch = function (props) {
  let classNames = ["switch", (props.switched) ? "switch_is-on" : "switch_is-off"].join(" ");
  return (
    <div className={classNames} onClick={props.handleSwitch}>
      <ToggleButton switched={props.switched} />
    </div>
  );
}

const ToggleButton = function (props) {
  let classNames = ["toggle-button", (props.switched) ? "toggle-button_position-right" : "toggle-button_position-left"].join(" ");
  return (<div className={classNames}></div>);
};





//export default PromoteStudentForm;

const mapStateToProps = state => ({
  academicSessionReport: state.academicSessionReport,
  admissionRegistrationReport: state.admissionRegistrationReport,
  promoteTheStudentResponse: state.promoteTheStudentResponse
})

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAcademicSessionReport: () => {
      dispatch(getAcademicSessionReport())
    },
    getAdmissionRegistrationReport: (dataToSend) => {
      dispatch(getAdmissionRegistrationReport(dataToSend))
    },
    promoteTheStudent: (dataToSend) => {
      dispatch(promoteTheStudent(dataToSend))
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(PromoteStudentForm);
