import React, { Component } from 'react';

import {
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock,
    Button,

} from 'react-bootstrap';

import { connect } from 'react-redux';
import { addHallInfo, updateHallInfo, getHallInfo } from '../actions/hall_actions';

import { SolidLoadingAnimation, TransparentLoadingAnimation } from '../utils/loadingAnimation.js'


class HallInfo extends Component {
    constructor(props) {
        super(props);

        var isThisEditVisit = false;
        if (this.props.match.params.id) {
            isThisEditVisit = true;
        }

        this.state = {
            hallTypeAvailableValues: ["Reading", "Digital", "jui"],
            userSelectedValues: {
                hallType: 'select',
                hallNumber: '',
                hallName: '',
                hallIntake: '',
            },
            isThisEdit: isThisEditVisit,
            isThisFirstTime: true,
            isSubmitted: false
        };
    }
    componentWillMount() {
        if (this.state.isThisEdit) {
            console.log("id is there>>" + this.state.isThisEdit);
            this.props.getHallInfo(this.props.match.params.id );
        } else {
            console.log("id is not there>>" + this.state.isThisEdit);
        }
    }

    validateAllFields() {
        var statusToReturn = true;
        var idsOfFieldsToValidate = ["hallType", "hallNo", "hallName", "hallIntake"]
        for (var i = 0; i < idsOfFieldsToValidate.length; i++) {
            var currentElement = document.getElementById(idsOfFieldsToValidate[i]);
            if (currentElement.value != "" && currentElement.value != "select") {//|| currentElement.value != "select"
                console.log("ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border:dummy")
            } else {
                console.log("not ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border: 1px solid #d44545")
                statusToReturn = false;
            }
        }
        if (this.validateNumberCheck()) {
            return statusToReturn;
        } else {
            return false;
        }
    }

    validateNumberCheck() {
        var regExForNumericalValue = /^[0-9]+$/;
        var statusToReturn = true;
        var idsOfFieldsToValidate = ["hallNo", "hallIntake"]
        for (var i = 0; i < idsOfFieldsToValidate.length; i++) {
            var currentElement = document.getElementById(idsOfFieldsToValidate[i]);
            if (regExForNumericalValue.test(currentElement.value)) {
                console.log("ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border:dummy")
            } else {
                console.log("not ok >> " + idsOfFieldsToValidate[i] + " >>" + currentElement.value);
                currentElement.setAttribute("style", "border: 1px solid #d44545")
                statusToReturn = false;
            }
        }
        return statusToReturn;
    }


    handleUserInputs(type, newValue) {
        if (type == "hallType") {
            this.state.userSelectedValues.hallType = newValue;
        } else if (type == "hallNumber") {
            this.state.userSelectedValues.hallNumber = newValue;
        } else if (type == "hallName") {
            this.state.userSelectedValues.hallName = newValue;
        } else if (type == "hallIntake") {
            this.state.userSelectedValues.hallIntake = newValue;
        }
        this.setState({ owner: "" }); //dummy but required
    }

    submitButtonClicked() {
        if (this.validateAllFields()) {
            this.setState({ isSubmitted: true });
            var dataToSend = {
                type: this.state.userSelectedValues.hallType != "select" ? this.state.userSelectedValues.hallType : "-",
                hall_id: this.state.userSelectedValues.hallNumber != "" ? this.state.userSelectedValues.hallNumber : "-",
                name: this.state.userSelectedValues.hallName != "" ? this.state.userSelectedValues.hallName : "-",
                intake: this.state.userSelectedValues.hallIntake != "" ? this.state.userSelectedValues.hallIntake : "-"
            }
            
            if (this.state.isThisEdit) {
                console.log("edit submitted dataToSend >>", dataToSend);
                this.props.updateHallInfo(this.props.match.params.id,dataToSend);
            } else {
                console.log("add submitted dataToSend >>", dataToSend);
                this.props.addHallInfo(dataToSend);
            }
        } else {
            alert("please fill fields");
        }
    }


    render() {
        if (this.props.getHallResponse.success == true && this.state.isThisFirstTime) {
            this.state.isThisFirstTime = false
            if (this.props.getHallResponse.responseData && this.props.getHallResponse.responseData.data && this.props.getHallResponse.responseData.data.rows) {
                if (this.props.getHallResponse.responseData.data.rows[0]) {
                    console.log("getHallResponse " + this.props.getHallResponse.responseData.data);
                    this.state.userSelectedValues.hallType = this.props.getHallResponse.responseData.data.rows[0].type;
                    this.state.userSelectedValues.hallNumber = this.props.getHallResponse.responseData.data.rows[0].hall_id;
                    this.state.userSelectedValues.hallName = this.props.getHallResponse.responseData.data.rows[0].name;
                    this.state.userSelectedValues.hallIntake = this.props.getHallResponse.responseData.data.rows[0].intake;
                }
            }
        }

        if (this.props.addHallResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("addHallResponse success");
        }

        if (this.props.updateHallResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("updateHallResponse success");
        }

        return (
            <div className="formWrapper">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="col-sm-12">
                            <div className="form-header" style={{ borderRadius: 3 }}>
                                {this.state.isThisEdit == true ? <h3 style={{ padding: 5 }}>Update HALL</h3>
                                    :
                                    <h3 style={{ padding: 5 }}>ADD HALL</h3>
                                }
                                <span className="subhead" style={{ padding: 5 }}>Hall Information</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <form className="info-form">
                            <div className="col-sm-4">
                                <label>Hall Type.</label>
                                <select id="hallType" value={this.state.userSelectedValues.hallType} className="form-control" onChange={(event) => this.handleUserInputs("hallType", event.target.value)}>
                                    <option value="select">Select</option>
                                    {this.state.hallTypeAvailableValues.map(temp =>
                                        <option value={temp}>{temp}</option>
                                    )}
                                </select>
                            </div>
                            <div className="col-sm-4">
                                <label>Hall No.</label>
                                <input id="hallNo" value={this.state.userSelectedValues.hallNumber} type="text" className="form-control" placeholder="Hall No." onChange={(event) => this.handleUserInputs("hallNumber", event.target.value)} />
                            </div>
                            <div className="col-sm-4">
                                <label>Hall Name</label>
                                <input id="hallName" value={this.state.userSelectedValues.hallName} type="text" className="form-control" placeholder="Hall Name" onChange={(event) => this.handleUserInputs("hallName", event.target.value)} />
                            </div>
                            <div className="col-sm-4">
                                <label>Intake</label>
                                <input id="hallIntake" value={this.state.userSelectedValues.hallIntake} type="text" className="form-control" placeholder="Intake" onChange={(event) => this.handleUserInputs("hallIntake", event.target.value)} />
                            </div>
                            <div className="col-sm-12" style={{ justifyContent: "center", flex: 1, display: "flex", flexDirection: "column", alignItems: "center", padding: 40 }}>
                                <Button bsStyle="primary" form="form" onClick={() => this.submitButtonClicked()} style={{ width: 100, color: "#fff" }}>Submit</Button>
                            </div>
                        </form>
                    </div>
                </div>
                <TransparentLoadingAnimation isVisible={this.props.addHallResponse.loading || this.props.updateHallResponse.loading || this.props.getHallResponse.loading} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    addHallResponse: state.addHallResponse,
    updateHallResponse: state.updateHallResponse,
    getHallResponse: state.getHallResponse
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        addHallInfo: (dataToSend) => {
            dispatch(addHallInfo(dataToSend))
        },
        updateHallInfo: (receivedId,dataToSend) => {
            dispatch(updateHallInfo(receivedId,dataToSend))
        },
        getHallInfo: (receivedId) => {
            dispatch(getHallInfo(receivedId))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HallInfo);
