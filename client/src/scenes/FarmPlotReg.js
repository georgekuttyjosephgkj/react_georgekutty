import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import '../res/scss/form.css';
import {
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock,
    Button,
} from 'react-bootstrap';

import { connect } from 'react-redux';
import { getFarmPlotReg, updateFarmPlotReg, addFarmPlotReg } from '../actions/farmPlotReg_actions';
import { getFarmReg } from '../actions/farmReg_actions';

import { SolidLoadingAnimation, TransparentLoadingAnimation } from '../utils/loadingAnimation.js'

class FarmPlotReg extends Component {
    constructor(props) {
        super(props)

        var isThisEditVisit = false;
        if (this.props.match.params.id) {
            isThisEditVisit = true;
        }

        this.state = {
            date: moment(),

            placeName: "",
            gatNumber: "",
            plotName: "",
            area: "",
            specification: "",
            legalText:"",
            farmRegistrationId:"",

            isThisEdit: isThisEditVisit,
            isThisFirstTime: true,
            isSubmitted: false,

            farmRegistrationIdAvailableValues:[]
        };
    }

    componentWillMount() {
        if (this.state.isThisEdit) {
            console.log("id is there>>" + this.state.isThisEdit);
            this.props.getFarmPlotReg({ id: this.props.match.params.id });
        } else {
            console.log("id is not there>>" + this.state.isThisEdit);
        }
        this.props.getFarmReg({ id:""});
    }

    handleChange(from, date) {
        console.log("from date", from, " ", date);
        this.setState({
            [from]: date
        });
    }

    validateAllFields() {
        var statusToReturn = true;
        var idsOfFieldsToValidate=["farmRegistration"]
        for(var i=0;i<idsOfFieldsToValidate.length;i++){
            var currentElement=document.getElementById(idsOfFieldsToValidate[i]);
            if(currentElement.value != "" && currentElement.value != "select"){//|| currentElement.value != "select"
                console.log("ok >> "+idsOfFieldsToValidate[i]+" >>"+currentElement.value);
                currentElement.setAttribute("style","border:dummy")
            }else{
                console.log("not ok >> "+idsOfFieldsToValidate[i]+" >>"+currentElement.value);
                currentElement.setAttribute("style","border: 1px solid #d44545")
                statusToReturn = false;
            }
        }
        return statusToReturn;
    }

    handleUserInputs(type, newValue) {
        console.log(type+" "+newValue)
        this.state[type] = newValue;
        this.setState({ dummy: "" });
    }

    submitButtonClicked() {
        if (this.validateAllFields()) {
            this.setState({ isSubmitted: true });
            var dataToSend = {
                plot_name: this.state.plotName,
                area: this.state.area,
                specification: this.state.specification,
                legal_text:this.state.legalText,
                farmRegistrationId:this.state.farmRegistrationId
            }
            console.log("dataToSend is >>", dataToSend);
            if (this.state.isThisEdit) {
                console.log("edit submitted dataToSend >>", dataToSend);
                this.props.updateFarmPlotReg(this.props.match.params.id, dataToSend);
            } else {
                console.log("add submitted dataToSend >>", dataToSend);
                this.props.addFarmPlotReg(dataToSend);
            }
        } else {
            alert("please fill forms");
        }
    }

    render() {

        if (this.props.getFarmRegResponse.success == true) {
            var responseArray = this.props.getFarmRegResponse.responseData.data.rows;
            var tempFarmRegistrationIdAvailableValues = [];
            for (var i = 0; i < responseArray.length; i++) {
                var tempObject = {};
                tempObject["id"] = responseArray[i].id;
                tempObject["placeName"] = responseArray[i].place_name;
                tempFarmRegistrationIdAvailableValues.push(tempObject);
            }
            console.log("tempFarmRegistrationIdAvailableValues >>", tempFarmRegistrationIdAvailableValues);
            this.state.farmRegistrationIdAvailableValues = tempFarmRegistrationIdAvailableValues;
        }
        
        if (this.props.getFarmPlotRegResponse.success == true && this.state.isThisFirstTime) {
            this.state.isThisFirstTime = false
            var responseObject = this.props.getFarmPlotRegResponse.responseData.data.rows[0];
            var farmRegistration = responseObject.farm_registration;
            console.log(" farmRegistration >>", farmRegistration);

            this.state.date = moment(farmRegistration.date);
            this.state.placeName = farmRegistration.place_name;
            this.state.gatNumber = farmRegistration.gat_no;
            this.state.plotName = responseObject.plot_name;
            this.state.area = responseObject.area;
            this.state.specification = responseObject.specification;
            this.state.farmRegistrationId = responseObject.farmRegistrationId;
            this.state.legalText = responseObject.legal_text;
        }


        if (this.props.addFarmPlotRegResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("addFarmPlotRegResponse success");
        }

        if (this.props.updateFarmPlotRegResponse.success && this.state.isSubmitted) {
            this.setState({ isSubmitted: false });
            alert("updateFarmPlotRegResponse success");
        }



        return (
            <div className="formWrapper">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="col-sm-12">
                            <div className="form-header">
                                <h3>FARM PLOT REGISTRATION</h3>
                                <span className="subhead">Plot Information</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <form className="info-form">
                            <div className="col-sm-4">
                                <label>Farm Registration Id</label>
                                <select id="farmRegistration" value={this.state.farmRegistrationId} onChange={(event) => this.handleUserInputs("farmRegistrationId", event.target.value)}  className="form-control">
                                    <option value="select">Select</option>
                                    {this.state.farmRegistrationIdAvailableValues.map(temp =>
                                        <option value={temp.id}>{temp.placeName}</option>
                                    )}
                                </select>
                               </div>
                            <div className="col-sm-4">
                                <label>Plot Name</label>
                                <input value={this.state.plotName} onChange={(event) => this.handleUserInputs("plotName", event.target.value)} type="text" className="form-control" placeholder="Plot Name"/>
                            </div>
                            <div className="col-sm-4">
                                <label>Area</label>
                                <input value={this.state.area} onChange={(event) => this.handleUserInputs("area", event.target.value)} type="text" className="form-control" placeholder="Area"/>
                            </div>
                            <div className="col-sm-4">
                                <label>Specification</label>
                                <input value={this.state.specification} onChange={(event) => this.handleUserInputs("specification", event.target.value)} type="text" className="form-control" placeholder="Specification"/>
                            </div>
                            <div className="col-sm-4">
                                <label>Legal Text</label>
                                <input value={this.state.legalText} onChange={(event) => this.handleUserInputs("legalText", event.target.value)} type="text" className="form-control" placeholder="Legal text"/>
                            </div>
                            <div className="col-sm-12" style={{ justifyContent: "center", flex: 1, display: "flex", flexDirection: "column", alignItems: "center", padding: 40 }}>
                                <Button type="submit" bsStyle="primary" form="form" onClick={() => this.submitButtonClicked()} style={{ width: 100, color: "#fff" }}>Submit</Button>
                            </div>
                        </form>
                    </div>
                </div>  
                <TransparentLoadingAnimation isVisible={this.props.getFarmRegResponse.loading || this.props.getFarmPlotRegResponse.loading || this.props.addFarmPlotRegResponse.loading || this.props.updateFarmPlotRegResponse.loading} />                          
            </div>
        )
    }
}

const mapStateToProps = state => ({
    getFarmRegResponse: state.getFarmRegResponse,
    getFarmPlotRegResponse: state.getFarmPlotRegResponse,
    addFarmPlotRegResponse: state.addFarmPlotRegResponse,
    updateFarmPlotRegResponse: state.updateFarmPlotRegResponse
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        getFarmReg: (dataToSend) => {
            dispatch(getFarmReg(dataToSend))
        },
        getFarmPlotReg: (dataToSend) => {
            dispatch(getFarmPlotReg(dataToSend))
        },
        addFarmPlotReg: (dataToSend) => {
            dispatch(addFarmPlotReg(dataToSend))
        },
        updateFarmPlotReg: (id, dataToSend) => {
            dispatch(updateFarmPlotReg(id, dataToSend))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FarmPlotReg);
