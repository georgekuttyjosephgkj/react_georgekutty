import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import '../res/scss/form.css';

class Qualification extends Component {
    constructor(props) {
        super(props)
        this.state = {
            qualificationDetailsValues: [
                {
                    qualification: "",
                    school: "",
                    university: "",
                    qualificationDate: moment(),
                    percentage: ""
                }
            ]
        };
    }

    addNewQualificationRow() {
        var tempQualificationDetailsValues = this.state.qualificationDetailsValues;
        tempQualificationDetailsValues.push({
            qualification: "",
            school: "",
            university: "",
            qualificationDate: moment(),
            percentage: ""
        });
        this.setState({ qualificationDetailsValues: tempQualificationDetailsValues });
    }

    removeQualificationRow(arrayIndexToRemove) {
        var tempQualificationDetailsValues = this.state.qualificationDetailsValues;
        tempQualificationDetailsValues.splice(arrayIndexToRemove, 1);
        this.setState({ qualificationDetailsValues: tempQualificationDetailsValues });
    }

    handleQualificationInputChange(receivedArrayIndex, type, newValue) {
        var tempQualificationDetailsValues = this.state.qualificationDetailsValues;
        tempQualificationDetailsValues[receivedArrayIndex][type] = newValue;
        this.setState({ qualificationDetailsValues: tempQualificationDetailsValues });
    }

    submitQualificationDetails() {
        var dataToSend = this.state.qualificationDetailsValues;
        for (var i = 0; i < dataToSend.length; i++) {
            dataToSend[i].qualificationDate = dataToSend[i].qualificationDate.format("YYYY-MM-DD");
        }
        this.setState({
            qualificationDetailsValues: [{
                qualification: "",
                school: "",
                university: "",
                qualificationDate: moment(),
                percentage: ""
            }]
        });
        console.log("you clicked qualification submit dataToSend>>", dataToSend);
    }

    renderQualificationUI() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="col-sm-12">
                        <div className="form-header">

                            <span className="subhead">Qualification Details </span>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12">
                    <form className="info-form">
                        <div className="col-sm-12">
                            <div className="col-sm-2"><label>Qualification</label></div>
                            <div className="col-sm-2"><label>School</label></div>
                            <div className="col-sm-2"><label>University/Board</label></div>
                            <div className="col-sm-2"><label>Month/Year</label></div>
                            <div className="col-sm-2"><label>Percentage</label></div>
                            <div className="col-sm-2">
                                <input className="btn btn-primary add" type='button' value='&nbsp;&nbsp;add&nbsp;' onClick={() => this.addNewQualificationRow()} />
                            </div>
                        </div>
                        {this.state.qualificationDetailsValues.map((temp, i) =>
                            <div className="col-sm-12" style={{ padding: 10 }}>
                                <div className="col-sm-2">
                                    <input type="text" onChange={(event) => this.handleQualificationInputChange(i, "qualification", event.target.value)} value={temp.qualification} />
                                </div>
                                <div className="col-sm-2">
                                    <input type="text" onChange={(event) => this.handleQualificationInputChange(i, "school", event.target.value)} value={temp.school} />
                                </div>
                                <div className="col-sm-2">
                                    <input type="text" onChange={(event) => this.handleQualificationInputChange(i, "university", event.target.value)} value={temp.university} />
                                </div>
                                <div className="col-sm-2">
                                    <DatePicker className="form-control" selected={temp.qualificationDate} onChange={(event) => this.handleQualificationInputChange(i, "qualificationDate", moment(event))} />
                                </div>
                                <div className="col-sm-2">
                                    <input type="text" onChange={(event) => this.handleQualificationInputChange(i, "percentage", event.target.value)} value={temp.percentage} />
                                </div>
                                <div className="col-sm-2">
                                    <input className="btn btn-primary" type='button' value='close' onClick={() => this.removeQualificationRow(i)} />
                                </div>
                            </div>
                        )}
                        <div className="col-sm-12" style={{ textAlign: "center" }} >
                            <input className="btn btn-primary" type='button' value='submit' onClick={() => this.submitQualificationDetails()} />
                        </div>
                    </form>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="formWrapper">
                {this.renderQualificationUI()}
            </div>
        )
    }
}

export default Qualification;
