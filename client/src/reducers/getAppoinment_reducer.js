const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'get Appoinment'
}
export default function getAppoinmentIReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "getAppoinmentSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "getAppoinmentFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "getAppoinmentLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}