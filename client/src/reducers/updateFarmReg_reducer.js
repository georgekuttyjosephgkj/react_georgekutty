const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'update FarmReg'
}
export default function updateFarmRegReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "updateFarmRegSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "updateFarmRegFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "updateFarmRegLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}