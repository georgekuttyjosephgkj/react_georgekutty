const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'get FarmPlot'
}
export default function getFarmPlotReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "getFarmPlotRegSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "getFarmPlotRegFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "getFarmPlotRegLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}