const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'get hall'
}
export default function getHallReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "getHallSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "getHallFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "getHallLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}