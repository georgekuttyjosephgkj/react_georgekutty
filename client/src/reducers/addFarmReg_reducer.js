const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'add FarmReg'
}
export default function addFarmRegReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "addFarmRegSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "addFarmRegFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "addFarmRegLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}