const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'get FarmReg'
}
export default function getFarmRegReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "getFarmRegSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "getFarmRegFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "getFarmRegLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}