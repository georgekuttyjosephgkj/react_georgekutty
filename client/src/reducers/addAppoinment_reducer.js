const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'add Appoinment'
}
export default function addAppoinmentReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "addAppoinmentInfoSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "addAppoinmentInfoFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "addAppoinmentInfoLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}