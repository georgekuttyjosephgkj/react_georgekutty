const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'update Appoinment'
}
export default function updateAppoinmentReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "updateAppoinmentSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "updateAppoinmentFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "updateAppoinmentLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}