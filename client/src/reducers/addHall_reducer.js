const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'add hall'
}
export default function addHallReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "addHallInfoSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "addHallInfoFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "addHallInfoLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}