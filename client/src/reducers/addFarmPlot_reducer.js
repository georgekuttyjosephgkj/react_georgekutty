const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'add FarmPlot'
}
export default function addFarmPlotReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "addFarmPlotRegSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "addFarmPlotRegFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "addFarmPlotRegLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}