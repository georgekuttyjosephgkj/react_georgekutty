const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'update hall'
}
export default function updateHallReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "updateHallSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "updateHallFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "updateHallLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}