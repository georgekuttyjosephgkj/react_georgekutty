const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'update FarmPlot'
}
export default function updateFarmPlotReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "updateFarmPlotRegSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "updateFarmPlotRegFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "updateFarmPlotRegLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}