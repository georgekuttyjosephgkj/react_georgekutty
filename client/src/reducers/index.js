import { combineReducers } from 'redux';
import list from './list';
import academicSessionReport from './academicSessionReport_reducer';
import admissionRegistrationReport from './admissionRegistrationReport_reducer';
import promoteTheStudentResponse from './promoteTheStudent_reducer';
import addHallResponse from './addHall_reducer';
import updateHallResponse from './updateHall_reducer';
import getHallResponse from './getHall_reducer';
import dischargeSubmitResponse from './discharge_reducer';
import addAppoinmentResponse from './addAppoinment_reducer';
import getAppoinmentResponse from './getAppoinment_reducer';
import updateAppoinmentResponse from './updateAppoinment_reducer';

import getFarmRegResponse from './getFarmReg_reducer';
import addFarmRegResponse from './addFarmReg_reducer';
import updateFarmRegResponse from './updateFarmReg_reducer';

import getFarmPlotRegResponse from './getFarmPlot_reducer';
import addFarmPlotRegResponse from './addFarmPlot_reducer';
import updateFarmPlotRegResponse from './updateFarmPlot_reducer';




const rootReducer = combineReducers({
  list, // shorthand for lists: lists
  academicSessionReport,
  admissionRegistrationReport,
  promoteTheStudentResponse,
  addHallResponse,
  updateHallResponse,
  getHallResponse,
  dischargeSubmitResponse,
  addAppoinmentResponse,
  getAppoinmentResponse,
  updateAppoinmentResponse,
  getFarmRegResponse,
  addFarmRegResponse,
  updateFarmRegResponse,
  getFarmPlotRegResponse,
  addFarmPlotRegResponse,
  updateFarmPlotRegResponse
});

export default rootReducer;
