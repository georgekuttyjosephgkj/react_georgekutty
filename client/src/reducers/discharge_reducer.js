const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'discharge submit'
}
export default function dischargeSubmitReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "submitDischargeSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "submitDischargeFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "submitDischargeLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}