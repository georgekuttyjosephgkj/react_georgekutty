const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'Admission Registration Report'
}
export default function admissionRegistrationReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "admissionRegistrationReportSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "admissionRegistrationReportFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "admissionRegistrationReportLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}