const initialState = {
    responseData:{},
    success:false,
    error: false,
    loading:false,
    message:'student promotion'
}
export default function admissionRegistrationReducer(state = initialState, {type,payload,message}) {
   switch(type){
       case "promoteTheStudentSuccess" :
           return {
               ...state,
               success:true,
               error: false,
               loading: false,
               responseData : payload.data,
               message
           }
       case "promoteTheStudentFailure" :
           return {
               ...state,
               success:false,
               error: true,
               loading: false,
               payload,
               message
           }
       case "promoteTheStudentLoading" :
           return {
               ...state,
               success:false,
               error: false,
               loading: true,
               message
           }
    default :
       return state;
   }
}