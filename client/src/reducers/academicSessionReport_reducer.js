const initialState = {
     responseData:{},
	 success:false,
	 error: false,
	 loading:false,
	 message:'Academic Session Report'
}
export default function academicSessionReportReducer(state = initialState, {type,payload,message}) {
	switch(type){
		case "academicSessionReportSuccess" :
        	return {
				...state,
				success:true,
				error: false,
				loading: false,
				responseData : payload.data,
				message
			}
		case "academicSessionReportFailure" :
			return {
				...state,
				success:false,
				error: true,
				loading: false,
				payload,
				message
			}
		case "academicSessionReportLoading" :
			return {
				...state,
				success:false,
				error: false,
				loading: true,
				message
			}
	 default :
	    return state;
    }
}