import axios from 'axios';

export const addHallInfoSuccess = payload => ({
  type: "addHallInfoSuccess",
  payload
});

export const addHallInfoFailure = payload => ({
  type: "addHallInfoFailure",
  payload
});

export const addHallInfoLoading = () => ({
  type: "addHallInfoLoading",
  message: "Loading"
});

export const addHallInfo = (dataToSend) => { return (dispatch) =>  {
    let res ={
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(addHallInfoLoading())
     axios.post('http://app.samvardhanngo.org:5000/api/libraryhall/add',dataToSend)
     .then(res => {
       console.log("response of addHallInfo >>",res.data)
        //if (res.status === 200) {
        if (true) {
          return dispatch(addHallInfoSuccess(res));
        }else{
          return dispatch(addHallInfoFailure(res))
        } 
    })
    .catch(err => {
      return dispatch(addHallInfoFailure(err));
    });
  }}

















  export const updateHallSuccess = payload => ({
    type: "updateHallSuccess",
    payload
  });
  
  export const updateHallFailure = payload => ({
    type: "updateHallFailure",
    payload
  });
  
  export const updateHallLoading = () => ({
    type: "updateHallLoading",
    message: "Loading"
  });
  
  export const updateHallInfo = (id,dataToSend) => { return (dispatch) =>  {
      let res ={
        status: 200,
        message: "Success",
        data: {}
      }
      dispatch(updateHallLoading())
       axios.post('http://app.samvardhanngo.org:5000/api/libraryhall/edit/'+id,dataToSend)
       .then(res => {
         console.log("response of updateHall >>",res.data)
          //if (res.status === 200) {
          if (true) {
            return dispatch(updateHallSuccess(res));
          }else{
            return dispatch(updateHallFailure(res))
          } 
      })
      .catch(err => {
        return dispatch(updateHallFailure(err));
      });
    }}
  






    export const getHallSuccess = payload => ({
        type: "getHallSuccess",
        payload
      });
      
      export const getHallFailure = payload => ({
        type: "getHallFailure",
        payload
      });
      
      export const getHallLoading = () => ({
        type: "getHallLoading",
        message: "Loading"
      });
      
      export const getHallInfo = (receivedId) => { return (dispatch) =>  {
          let res ={
            status: 200,
            message: "Success",
            data: {}
          }
          dispatch(getHallLoading())
           axios.get('http://app.samvardhanngo.org:5000/api/libraryhall/getreport/'+receivedId)
           .then(res => {
             console.log("response of getHall >>",res.data)
              // if (res.status === 200) {
              if (true) {
                return dispatch(getHallSuccess(res));
              }else{
                return dispatch(getHallFailure(res))
              } 
          })
          .catch(err => {
            return dispatch(getHallFailure(err));
          });
        }}
      
    
    