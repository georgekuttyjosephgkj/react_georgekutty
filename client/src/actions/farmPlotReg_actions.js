import axios from 'axios';

export const addFarmPlotRegSuccess = payload => ({
  type: "addFarmPlotRegSuccess",
  payload
});

export const addFarmPlotRegFailure = payload => ({
  type: "addFarmPlotRegFailure",
  payload
});

export const addFarmPlotRegLoading = () => ({
  type: "addFarmPlotRegLoading",
  message: "Loading"
});

export const addFarmPlotReg = (dataToSend) => {
  return (dispatch) => {
    let res = {
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(addFarmPlotRegLoading())
    axios.post('http://app.samvardhanngo.org:5000/api/farm-plot/add', dataToSend)
      .then(res => {
        console.log("response of addFarmPlotReg >>", res.data)
        //if (res.status === 200) {
        if (true) {
          return dispatch(addFarmPlotRegSuccess(res));
        } else {
          return dispatch(addFarmPlotRegFailure(res))
        }
      })
      .catch(err => {
        return dispatch(addFarmPlotRegFailure(err));
      });
  }
}




export const updateFarmPlotRegSuccess = payload => ({
  type: "updateFarmPlotRegSuccess",
  payload
});

export const updateFarmPlotRegFailure = payload => ({
  type: "updateFarmPlotRegFailure",
  payload
});

export const updateFarmPlotRegLoading = () => ({
  type: "updateFarmPlotRegLoading",
  message: "Loading"
});

export const updateFarmPlotReg = (id, dataToSend) => {
  return (dispatch) => {
    let res = {
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(updateFarmPlotRegLoading())
    axios.post('http://app.samvardhanngo.org:5000/api/farm-plot/edit/' + id, dataToSend)
      .then(res => {
        console.log("response of updateFarmPlotReg >>", res.data)
        //if (res.status === 200) {
        if (true) {
          return dispatch(updateFarmPlotRegSuccess(res));
        } else {
          return dispatch(updateFarmPlotRegFailure(res))
        }
      })
      .catch(err => {
        return dispatch(updateFarmPlotRegFailure(err));
      });
  }
}







export const getFarmPlotRegSuccess = payload => ({
  type: "getFarmPlotRegSuccess",
  payload
});

export const getFarmPlotRegFailure = payload => ({
  type: "getFarmPlotRegFailure",
  payload
});

export const getFarmPlotRegLoading = () => ({
  type: "getFarmPlotRegLoading",
  message: "Loading"
});

export const getFarmPlotReg = (dataToSend) => {
  return (dispatch) => {
    let res = {
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(getFarmPlotRegLoading())
    axios.get('http://app.samvardhanngo.org:5000/api/farm-plot/getreport/' + dataToSend.id)
      .then(res => {
        console.log("response of getFarmPlotReg >>", res.data)
        // if (res.status === 200) {
        if (true) {
          return dispatch(getFarmPlotRegSuccess(res));
        } else {
          return dispatch(getFarmPlotRegFailure(res))
        }
      })
      .catch(err => {
        return dispatch(getFarmPlotRegFailure(err));
      });
  }
}


