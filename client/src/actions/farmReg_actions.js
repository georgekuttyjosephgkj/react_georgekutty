import axios from 'axios';

export const addFarmRegSuccess = payload => ({
  type: "addFarmRegSuccess",
  payload
});

export const addFarmRegFailure = payload => ({
  type: "addFarmRegFailure",
  payload
});

export const addFarmRegLoading = () => ({
  type: "addFarmRegLoading",
  message: "Loading"
});

export const addFarmReg = (dataToSend) => {
  return (dispatch) => {
    let res = {
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(addFarmRegLoading())
    axios.post('http://app.samvardhanngo.org:5000/api/farmreg/add', dataToSend)
      .then(res => {
        console.log("response of addFarmReg >>", res.data)
        //if (res.status === 200) {
        if (true) {
          return dispatch(addFarmRegSuccess(res));
        } else {
          return dispatch(addFarmRegFailure(res))
        }
      })
      .catch(err => {
        return dispatch(addFarmRegFailure(err));
      });
  }
}




export const updateFarmRegSuccess = payload => ({
  type: "updateFarmRegSuccess",
  payload
});

export const updateFarmRegFailure = payload => ({
  type: "updateFarmRegFailure",
  payload
});

export const updateFarmRegLoading = () => ({
  type: "updateFarmRegLoading",
  message: "Loading"
});

export const updateFarmReg = (id, dataToSend) => {
  return (dispatch) => {
    let res = {
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(updateFarmRegLoading())
    axios.post('http://app.samvardhanngo.org:5000/api/farmreg/edit/' + id, dataToSend)
      .then(res => {
        console.log("response of updateFarmReg >>", res.data)
        //if (res.status === 200) {
        if (true) {
          return dispatch(updateFarmRegSuccess(res));
        } else {
          return dispatch(updateFarmRegFailure(res))
        }
      })
      .catch(err => {
        return dispatch(updateFarmRegFailure(err));
      });
  }
}







export const getFarmRegSuccess = payload => ({
  type: "getFarmRegSuccess",
  payload
});

export const getFarmRegFailure = payload => ({
  type: "getFarmRegFailure",
  payload
});

export const getFarmRegLoading = () => ({
  type: "getFarmRegLoading",
  message: "Loading"
});

export const getFarmReg = (dataToSend) => {
  return (dispatch) => {
    let res = {
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(getFarmRegLoading())
    axios.get('http://app.samvardhanngo.org:5000/api/farmreg/getreport/' + dataToSend.id)
      .then(res => {
        console.log("response of getFarmReg >>", res.data)
        // if (res.status === 200) {
        if (true) {
          return dispatch(getFarmRegSuccess(res));
        } else {
          return dispatch(getFarmRegFailure(res))
        }
      })
      .catch(err => {
        return dispatch(getFarmRegFailure(err));
      });
  }
}


