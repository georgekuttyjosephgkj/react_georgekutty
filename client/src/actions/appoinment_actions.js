import axios from 'axios';

export const addAppoinmentInfoSuccess = payload => ({
  type: "addAppoinmentInfoSuccess",
  payload
});

export const addAppoinmentInfoFailure = payload => ({
  type: "addAppoinmentInfoFailure",
  payload
});

export const addAppoinmentInfoLoading = () => ({
  type: "addAppoinmentInfoLoading",
  message: "Loading"
});

export const addAppoinmentInfo = (dataToSend) => { return (dispatch) =>  {
    let res ={
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(addAppoinmentInfoLoading())
     axios.post('http://app.samvardhanngo.org:5000/api/hremployeeappointment/add',dataToSend)
     .then(res => {
       console.log("response of addAppoinmentInfo >>",res.data)
        //if (res.status === 200) {
        if (true) {
          return dispatch(addAppoinmentInfoSuccess(res));
        }else{
          return dispatch(addAppoinmentInfoFailure(res))
        } 
    })
    .catch(err => {
      return dispatch(addAppoinmentInfoFailure(err));
    });
  }}




  export const updateAppoinmentSuccess = payload => ({
    type: "updateAppoinmentSuccess",
    payload
  });
  
  export const updateAppoinmentFailure = payload => ({
    type: "updateAppoinmentFailure",
    payload
  });
  
  export const updateAppoinmentLoading = () => ({
    type: "updateAppoinmentLoading",
    message: "Loading"
  });
  
  export const updateAppoinmentInfo = (id,dataToSend) => { return (dispatch) =>  {
      let res ={
        status: 200,
        message: "Success",
        data: {}
      }
      dispatch(updateAppoinmentLoading())
       axios.post('http://app.samvardhanngo.org:5000/api/hremployeeappointment/edit/'+id,dataToSend)
       .then(res => {
         console.log("response of updateAppoinment >>",res.data)
          //if (res.status === 200) {
          if (true) {
            return dispatch(updateAppoinmentSuccess(res));
          }else{
            return dispatch(updateAppoinmentFailure(res))
          } 
      })
      .catch(err => {
        return dispatch(updateAppoinmentFailure(err));
      });
    }}
  






    export const getAppoinmentSuccess = payload => ({
        type: "getAppoinmentSuccess",
        payload
      });
      
      export const getAppoinmentFailure = payload => ({
        type: "getAppoinmentFailure",
        payload
      });
      
      export const getAppoinmentLoading = () => ({
        type: "getAppoinmentLoading",
        message: "Loading"
      });
      
      export const getAppoinmentInfo = (dataToSend) => { return (dispatch) =>  {
          let res ={
            status: 200,
            message: "Success",
            data: {}
          }
          dispatch(getAppoinmentLoading())
           axios.get('http://app.samvardhanngo.org:5000/api/hremployeeappointment/getreport/'+dataToSend.id)
           .then(res => {
             console.log("response of getAppoinment >>",res.data)
              // if (res.status === 200) {
              if (true) {
                return dispatch(getAppoinmentSuccess(res));
              }else{
                return dispatch(getAppoinmentFailure(res))
              } 
          })
          .catch(err => {
            return dispatch(getAppoinmentFailure(err));
          });
        }}
      
    
    