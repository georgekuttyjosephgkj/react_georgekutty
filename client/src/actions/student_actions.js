import axios from 'axios';

export const academicSessionReportSuccess = payload => ({
  type: "academicSessionReportSuccess",
  payload
});

export const academicSessionReportFailure = payload => ({
  type: "academicSessionReportFailure",
  payload
});

export const academicSessionReportLoading = () => ({
  type: "academicSessionReportLoading",
  message: "Loading"
});

export const getAcademicSessionReport = () => { return (dispatch) =>  {
    let res ={
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(academicSessionReportLoading())
     axios.get('http://app.samvardhanngo.org:5000/api/academic-session/getreport')
     .then(res => {
       console.log("response of academicSessionReport >>",res.data)
        if (res.status === 200) {
          return dispatch(academicSessionReportSuccess(res));
        }else{
          return dispatch(academicSessionReportFailure(res))
        } 
    })
    .catch(err => {
      return dispatch(academicSessionReportFailure(err));
    });
  }}


export const admissionRegistrationReportSuccess = payload => ({
  type: "admissionRegistrationReportSuccess",
  payload
});

export const admissionRegistrationReportFailure = payload => ({
  type: "admissionRegistrationReportFailure",
  payload
});

export const admissionRegistrationReportLoading = () => ({
  type: "admissionRegistrationReportLoading",
  message: "Loading"
});

export const getAdmissionRegistrationReport = (dataToSend) => { return (dispatch) =>  {
    let res ={
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(admissionRegistrationReportLoading())
    axios.get('http://app.samvardhanngo.org:5000/api/admission-registration/getreport',dataToSend)
    //axios.post('studentPromotion/admissionRegReport',dataToSend)
     .then(res => {
       console.log("response of admissionRegistrationReport >>",res.data)
        if (res.status === 200) {
          return dispatch(admissionRegistrationReportSuccess(res));
        }else{
          return dispatch(admissionRegistrationReportFailure(res))
        } 
    })
    .catch(err => {
      return dispatch(admissionRegistrationReportFailure(err));
    });
  }}


export const promoteTheStudentSuccess = payload => ({
  type: "promoteTheStudentSuccess",
  payload
});

export const promoteTheStudentFailure = payload => ({
  type: "promoteTheStudentFailure",
  payload
});

export const promoteTheStudentLoading = () => ({
  type: "promoteTheStudentLoading",
  message: "Loading"
});

export const promoteTheStudent = (dataToSend) => { return (dispatch) =>  {
    let res ={
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(promoteTheStudentLoading())
     axios.post('http://app.samvardhanngo.org:5000/api/admission-registration/promote/add',dataToSend)
     .then(res => {
       console.log("response of promoteTheStudent >>",res.data)
        //if (res.status === 200) {
        if (res.data.status == "success") {
          console.log("studentPromotion inside success");
          return dispatch(promoteTheStudentSuccess(res));
        }else{
          console.log("studentPromotion inside fail");
          return dispatch(promoteTheStudentFailure(res))
        } 
    })
    .catch(err => {
      return dispatch(promoteTheStudentFailure(err));
    });
  }}

