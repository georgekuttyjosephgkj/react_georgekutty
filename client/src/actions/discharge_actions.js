import axios from 'axios';

export const submitDischargeSuccess = payload => ({
  type: "submitDischargeSuccess",
  payload
});

export const submitDischargeFailure = payload => ({
  type: "submitDischargeFailure",
  payload
});

export const submitDischargeLoading = () => ({
  type: "submitDischargeLoading",
  message: "Loading"
});

export const submitDischargeInfo = (receivedId, dataToSend) => {
  return (dispatch) => {
    let res = {
      status: 200,
      message: "Success",
      data: {}
    }
    dispatch(submitDischargeLoading())
    axios.post('http://app.samvardhanngo.org:5000/api/hremployee-registration/deadmit/' + receivedId, dataToSend)
      .then(res => {
        console.log("response of submitDischarge >>", res.data)
        // if (res.status === 200) {
        if (true) {
          return dispatch(submitDischargeSuccess(res));
        } else {
          return dispatch(submitDischargeFailure(res))
        }
      })
      .catch(err => {
        return dispatch(submitDischargeFailure(err));
      });
  }
}


