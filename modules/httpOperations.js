/*
 ╔═╗┌─┐┌─┐┬─┐┌─┐┌─┐┬┌─┬ ┬┌┬┐┌┬┐┬ ┬
 ║ ╦├┤ │ │├┬┘│ ┬├┤ ├┴┐│ │ │  │ └┬┘
 ╚═╝└─┘└─┘┴└─└─┘└─┘┴ ┴└─┘ ┴  ┴  ┴ 
 Designed and developed by Georgekutty with ❤ on react
*/

const axios = require('axios');

module.exports.executeAxiosGet = function (url,callback) {
    axios.get(url)
        .then(response => {
            callback(response)
        })
        .catch(error => {
            console.log(error);
        });
}

module.exports.executeAxiosPost = function (url,dataToSend,callback) {
    axios.post(url,dataToSend)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
}

